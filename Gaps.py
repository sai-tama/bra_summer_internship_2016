import pandas as pd
import numpy as np
from math import *
from BaseModule import *

def ScanHistory(table, today_trend, jump, stop_loss):
	if today_trend == 'Bullish':
		trend = False
	elif today_trend == 'Bearish':
		trend = True
	total_gain = []	
	table = table.reset_index(drop = True)
	for i,row in table.iterrows():
		i+=1
		if i == len(table)-4:
			break
		if GetSignal(table, today_trend, i, jump):
			my_gain = []
			my_gain = gain(table.ix[i-1:i],i,trend,stop_loss, jump)
			if my_gain is not None:
				total_gain.append(my_gain)
	return total_gain	

def GetSignal(table, trend, i, jump):
	
	if table.Trend.ix[i] != trend:
		return False	
	if trend == 'Bullish':
		entry = table.Open.ix[i] + table.Open.ix[i]*jump
		if table.High.ix[i] < entry:
			return False
	elif trend == 'Bearish':
		entry = table.Open.ix[i] - table.Open.ix[i]*jump
		if table.Low.ix[i] > entry:
			return False		
	return True

def OutputAnswer(ans, today_trend):
	if today_trend == 'Bullish':
		ans['Go up?'] = 'Up'
		ans['Action'] = 'Short'
	elif today_trend == 'Bearish':
		ans['Go up?'] = 'Down'
		ans['Action'] = 'Long'
	return ans
	
def GetForecast	(ticker,today_trend, jump, stop_loss, today_scen = -1):
	table = pd.read_csv('fast_Gaps_Forecasts_2010-2014.csv')
	return table[(table.Ticker == ticker) & (table.Scenario == today_scen) & (table.Trend == today_trend) & (table.Jump == jump) & (table.stop_loss == stop_loss)]
	
def checker(ticker, table, jump, stop_loss):
	i = len(table)-1
	today_trend = table.Trend.ix[i]
	#today_rsi = table.RSI.ix[i]
	if GetSignal(table,today_trend,i, jump):
		ans = GetForecast(ticker, today_trend, jump, stop_loss)
	else:
		return None
	if ans.empty or int(ans['Signal']) == 0:
		return None
	return OutputAnswer(ans,today_trend)
	

def tester(ticker,today_trend, jump, stop_loss):
	table = pd.read_csv('Tickers/Global/'+ticker+'.csv')	
	table = table[1007:len(table)-252]
	final = pd.DataFrame()
	total_gain = []
	total_gain = ScanHistory(table, today_trend, jump, stop_loss)
	ans=sharp(total_gain)
	ans['Ticker'] = ticker
	ans['Trend'] = today_trend
	ans['Scenario'] = -1
	ans['Jump'] = jump
	ans['stop_loss'] = stop_loss
	return ans

#print tester('FCX','Bullish',0.02,-0.02)

import dayX
import Gaps
from tqdm import tqdm
import pandas as pd
import numpy as np
from BaseModule import Timer

def getDayX(tickers):
	final = pd.DataFrame()
	test = pd.DataFrame()
	for ticker in tqdm(tickers):
		for days in range(5,10):
			test = strategy.tester(ticker,'Bullish',days)
			final = final.append(test,ignore_index = True)
			test = strategy.tester(ticker,'Bearish',days)
			final = final.append(test,ignore_index = True)
			print final
	#final.to_csv('fast_DayX_Forecasts_2010-2014.csv')	
			final.to_csv('med_DayX_Forecasts_2010-2014.csv')	
	print final
@Timer	
def getGaps(tickers, strategy):
	final = pd.DataFrame()
	test = pd.DataFrame()
	for ticker in tqdm(tickers):
		for jump in np.arange(0.005,0.021,0.005):
			for stop_loss in np.arange(-0.01,-0.026,-0.005):
				test = strategy.tester(ticker,'Bullish',jump, stop_loss)
				final = final.append(test,ignore_index = True)
				test = strategy.tester(ticker,'Bearish',jump, stop_loss)
				final = final.append(test,ignore_index = True)
	final.to_csv('fast_Gaps_Forecasts_2010-2014.csv')		
	print final
	
def main(strategy):
	Path_tickersFile = 'Tickers/fast.txt'
	Path_csvFile = 'Tickers/Global'
	tickers = []
	with open(Path_tickersFile) as f:
		for ticker in f.readlines():
			ticker = ticker[:-1]
			tickers.append(ticker)	
	
	#getDayX(tickers)
	getGaps(tickers, strategy)

	
main(Gaps)

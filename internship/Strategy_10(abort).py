#%%
import csv
import threading
import Queue
import numpy as np

from pandas.io.data import DataReader
from itertools import product

#%% 
#download data

with open('constituents.csv', 'rb') as f:
    reader = csv.reader(f)
    tickers=list(reader)
ticks=[]
ts=[]
for i in range(0,504):
    ts.extend(tickers[i])

start="2006/1/1"

exitFlag = 0

data=[]

class myThread (threading.Thread):
    def __init__(self, threadID, name, q):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.q = q
    def run(self):
        print "Starting " + self.name
        process_data(self.name, self.q, ticks)
        queueLock.acquire(blocking=False)        
        print "Exiting " + self.name
        queueLock.release()

def process_data(threadName, q,ticks):
    while not exitFlag:
        queueLock.acquire()
        if not workQueue.empty():
            t = q.get()
            print threadName+" downloading data of %s" % t
            queueLock.release()
            temp=DataReader(t,"yahoo",start)
            queueLock.acquire()
            data.append(temp)
            ticks.append(t)
            queueLock.release()            
        else:
            queueLock.release()


threadList = ["Thread-1", "Thread-2", "Thread-3","Thread-4", "Thread-5", "Thread-6","Thread-7", "Thread-8", "Thread-9","Thread-10","Thread-11", "Thread-12", "Thread-13","Thread-14", "Thread-15", "Thread-16","Thread-17", "Thread-18", "Thread-19","Thread-20","Thread-21", "Thread-22", "Thread-23","Thread-24", "Thread-25", "Thread-26","Thread-27", "Thread-28", "Thread-29","Thread-30","Thread-31", "Thread-32", "Thread-33","Thread-34", "Thread-35", "Thread-36","Thread-37", "Thread-38", "Thread-39","Thread-40"]
#nameList = ["One", "Two", "Three", "Four", "Five"]
queueLock = threading.Lock()
workQueue = Queue.Queue(510)
threads = []
threadID = 1

# Create new threads
for tName in threadList:
    thread = myThread(threadID, tName, workQueue)
    thread.start()
    threads.append(thread)
    threadID += 1

# Fill the queue
queueLock.acquire()
for ticker in ts:
    workQueue.put(ticker)
queueLock.release()

# Wait for queue to empty
while not workQueue.empty():
    pass

# Notify threads it's time to exit
exitFlag = 1

# Wait for all threads to complete
for t in threads:
    t.join()
print "Exiting Main Thread"

print 'Calculating RSI'

# calculates RSI
for stock in data:
    index = 0
    rsi = []
    poschange = []
    negchange = []
    avggain = []
    avgloss = []
    close = stock['Adj Close'].tolist()
    for day in close:
        if index == 0:
            poschange.append('-')
            negchange.append('-')
        elif (close[index] - close[index - 1]) > 0.0:
            poschange.append(close[index] - close[index - 1])
            negchange.append(0.0)
        else:
            poschange.append(0.0)
            negchange.append(close[index - 1] - close[index])
        if index == 0:
            avggain.append('-')
            avgloss.append('-')
        elif index == 1:
            avggain.append(poschange[index])
            avgloss.append(negchange[index])
        elif index <= 14:
            avggain.append(((avggain[index - 1]*(index - 1) + poschange[index])/index))
            avgloss.append(((avgloss[index - 1]*(index - 1) + negchange[index])/index))
        else:
            avggain.append(((avggain[index - 1]*13) + poschange[index])/14)
            avgloss.append(((avgloss[index - 1]*13) + negchange[index])/14)
        if index < 14:
            rsi.append('-')
        elif avgloss[index] == 0:
            rsi.append('-')
        else:
            rsi.append(100 - (100/(1+((avggain[index])/(avgloss[index])))))     
        index += 1
    stock['RSI'] = rsi

print "Calculating ADX and Scenarios"

#Calculates ADX and Scenarios
for stock in data:
    high = stock.High.tolist()
    low = stock.Low.tolist()
    close = stock.Close.tolist()
    rsi = stock.RSI.tolist()
    index = 0
    TR14 = []
    PDM14 = []
    MDM14 = []
    PDI14 = []
    MDI14 = []
    DX= []
    ADX = []
    Scenario = []
    for i in range(14): 
        TR14.append(0), PDM14.append(0),MDM14.append(0),PDI14.append(0),
        MDI14.append(0), DX.append(0), ADX.append("-")
    for i in range(13): ADX.append("-")
    TR=[0]
    #+DM and -DM 
    PDM =[0]
    MDM =[0]
    for day in high:
        if index>0:
            TR.append(max([high[index]-low[index],abs(high[index]-close[index-1]),abs(low[index]-close[index-1])]))
            TR[index] = round(TR[index]*100)/100
            if high[index]-high[index-1]>low[index-1]-low[index] and high[index]-high[index-1]>0:
                PDM.append(high[index]-high[index-1])
            else: PDM.append(0)
            PDM[index] = round(PDM[index]*100)/100
            if high[index]-high[index-1]<low[index-1]-low[index] and low[index-1]-low[index]>0:
                MDM.append(low[index-1]-low[index])
            else: MDM.append(0)
            MDM[index] = round(MDM[index]*100)/100
        if index==14: 
            TR14.append(sum(TR)),MDM14.append(sum(MDM)),PDM14.append(sum(PDM))
            PDI14.append(PDM14[index]/TR14[index]*100)
            MDI14.append(MDM14[index]/TR14[index]*100)
            DX.append(abs(PDI14[index]-MDI14[index])/(PDI14[index]+MDI14[index])*100)
        if index >14: 
            TR14.append(TR14[index-1]-TR14[index-1]/14+TR[index])
            MDM14.append(MDM14[index-1]-MDM14[index-1]/14+MDM[index])
            PDM14.append(PDM14[index-1]-PDM14[index-1]/14+PDM[index])
            PDI14.append(PDM14[index]/TR14[index]*100)
            MDI14.append(MDM14[index]/TR14[index]*100)
            DX.append(abs(PDI14[index]-MDI14[index])/(PDI14[index]+MDI14[index])*100)
        if index==27: ADX.append(sum(DX)/14)
        if index>27:  ADX.append((13*ADX[index-1]+DX[index])/14)
        TR14[index] = round(TR14[index]*100)/100
        MDM14[index] = round(MDM14[index]*100)/100
        PDM14[index] = round(PDM14[index]*100)/100
        MDI14[index] = round(MDI14[index]*100)/100
        PDI14[index] = round(PDI14[index]*100)/100
        DX[index] = round(DX[index]*100)/100
        if ADX[index] <20:
            if rsi[index] > 70:
                Scenario.append(2)
            elif rsi[index] < 30:
                Scenario.append(8)
            else:
                Scenario.append(5)
        elif ADX[index]>25 and ADX[index]!='-' and PDI14[index]>MDI14[index]:
            if rsi[index] > 70:
                Scenario.append(1)
            elif rsi[index] < 30:
                Scenario.append(7)
            else:
                Scenario.append(4)
        elif ADX[index]>25 and ADX[index]!='-' and PDI14[index]<MDI14[index]:
            if rsi[index] > 70:
                Scenario.append(3)
            elif rsi[index] < 30:
                Scenario.append(9)
            else:
                Scenario.append(6)
        else: Scenario.append(0)
        try:ADX[index] = round(ADX[index]*100)/100
        except: 
            index +=1 
            continue
        index +=1
        
    if len(high)<27:
            ADX =[]
            for i in range(len(high)):
                ADX.append("-")    
    stock['ADX']= ADX
    stock['Scenario']=Scenario

print 'Calculating Consecutive Daily Change'

for stock in data:
    close=stock.Close.tolist()
    Open=stock.Open.tolist()
    index=0
    streak=[]
    streakreturn=[]
    for day in close:
        if index==0:
            if close[index]>=Open[index]:
                streak.append(1)
                streakreturn.append(close[index]/Open[index] - 1)
            else:
                streak.append(-1)
                streakreturn.append(close[index]/Open[index] - 1)
        else:
            if streak[index-1]>0:
                if close[index]>=Open[index]:
                    streak.append(streak[index-1]+1)
                    streakreturn.append(close[index]/Open[index-streak[index]+1] - 1)
                else:
                    streak.append(-1)
                    streakreturn.append(close[index]/Open[index] - 1)
            else:
                if close[index]<=Open[index]:
                    streak.append(streak[index-1]-1)
                    streakreturn.append(close[index]/Open[index+streak[index]+1] - 1)
                else:
                    streak.append(1)
                    streakreturn.append(close[index]/Open[index] - 1)
        index +=1
    stock['Streak']=streak
    stock['StreakReturn']=streakreturn


#%%performance analysis function
    
def sharpe(ret):
    r=np.asarray(ret)
    return r.mean()/r.std()*np.sqrt(252)
    
#%%------------------------------Stategy 9----------------------
#Signal Iterations

def find_sharpe_bullish(price,days):       
    open_price = price.Open.tolist()
    close_price = price.Close.tolist()
    high_price = price.High.tolist()
    low_price = price.Low.tolist()
    scenario_price = price.Scenario.tolist()
    streak = price.Streak.tolist()
    streakreturn = price.StreakReturn.tolist()
    end = len(open_price)
    entry = [0.01,0.015,0.02,0.025,0.03,0.035] 
    stop_loss = [0.01,0.015,0.02,0.025]
    price_target = [0,0.0025,0.005]
    multiplier=[0.005,.01,.015,.02]
    scenario=[3,6]
    tuple_list = list(product(entry,stop_loss,price_target,multiplier,scenario))
    max_sharpe=0
    max_sharpe_param=tuple_list[0]
    ave_signal_ret=0
    num_signal=0
    for param in tuple_list:
        sig=0
        ret = []
        entry = param[0]
        stop_loss = param[1]
        price_target = param[2]
        multiplier=param[3]
        scenario=param[4]
    
        for t in range (1, end):
#bullish,short      
            if streak[t-1]>=days and streakreturn[t-1]/streak[t-1]>multiplier and scenario_price[t-1]==scenario:
                
                if high_price[t]>=open_price[t]*(1+entry):
                    sig+=1
                    short_price = open_price[t]*(1+entry)
                    exit_target = close_price[t-1]*(1+price_target)
                    if high_price[t]<short_price*(1+stop_loss):
                        if low_price[t]<exit_target:
                            cover_price=exit_target
                        else:
                            cover_price=close_price[t]
                    else:
                        cover_price=short_price*(1+stop_loss)
                    ret.append(1 - (cover_price/short_price))
            else:
                pass
        if sharpe(ret)>max_sharpe:
                max_sharpe=sharpe(ret)
                max_sharpe_param=param
                sum_of_ret=0
                for i in range(len(ret)):
                    sum_of_ret+=ret[i]
                ave_signal_ret=sum_of_ret/float(sig)
                num_signal=sig

    return (max_sharpe,ave_signal_ret,max_sharpe_param,num_signal)
    
#%%
def find_sharpe_bearish(price,days):       
    open_price = price.Open.tolist()
    close_price = price.Close.tolist()
    high_price = price.High.tolist()
    low_price= price.Low.tolist()
    scenario_price=price.Scenario.tolist()
    streak = price.Streak.tolist()
    streakreturn = price.StreakReturn.tolist()
    end = len(open_price)
    entry = [0.01,0.015,0.02,0.025,0.03,0.035] 
    stop_loss = [0.01,0.015,0.02,0.025]
    price_target = [0,0.0025,0.005]
    multiplier=[0.005,.01,.015,.02]
    scenario=[4,7]
    tuple_list = list(product(entry,stop_loss,price_target,multiplier,scenario))
    max_sharpe=0
    max_sharpe_param=tuple_list[0]
    ave_signal_ret=0
    num_signal=0;
    for param in tuple_list:
        sig=0
        ret = []
        entry = param[0]
        stop_loss = param[1]
        price_target = param[2]
        multiplier=param[3]
        scenario=param[4]
        
        for t in range (1, end):
#bearish, long    
            if streak[t-1]<=-days and streakreturn[t-1]/streak[t-1]>multiplier and scenario_price[t-1]==scenario:
                
                if low_price[t]<=open_price[t]*(1-entry):
                    sig+=1
                    buy_price = open_price[t]*(1-entry)
                    exit_target = close_price[t-1]*(1-price_target)
                    if low_price[t]>buy_price*(1-stop_loss):
                        if high_price[t]>exit_target:
                            sell_price=exit_target
                        else:
                            sell_price=close_price[t]
                    else:
                        sell_price=buy_price*(1-stop_loss)
                    ret.append(sell_price/buy_price - 1)
            else:
                pass

        if sharpe(ret)>max_sharpe:
                max_sharpe=sharpe(ret)
                max_sharpe_param=param
                sum_of_ret=0
                for i in range(len(ret)):
                    sum_of_ret+=ret[i]
                ave_signal_ret=sum_of_ret/float(sig)
                num_signal=sig

    return (max_sharpe,ave_signal_ret,max_sharpe_param,num_signal)


#%% qualified stocks   
 
def qs_list(year,data,ticks,days):
    x=np.zeros(1008,dtype={'names':['ticker','sharpe','ave_signal_ret','entry','stop_loss','price_target','multiplier','scenario','num_signal','bullish'],'formats':['S10','float16','float16','float16','float16','float16','float16','i2','i2','b1']})
    for i in range(504):
        #print 'calculating sharpe for %s' %ticks[i]
        v=find_sharpe_bullish(data[i].loc[str(year-5)+'0101':str(year-1)+'1231'],days)
        x[i]=(ticks[i],v[0],v[1],v[2][0],v[2][1],v[2][2],v[2][3],v[2][4],v[3],True)
        v=find_sharpe_bearish(data[i].loc[str(year-5)+'0101':str(year-1)+'1231'],days)
        x[i+504]=(ticks[i],v[0],v[1],v[2][0],v[2][1],v[2][2],v[2][3],v[2][4],v[3],False)
    performance=np.sort(x,order='sharpe')[::-1]#historical stat including('ticker','sharpe','ave_signal_ret','entry','stop_loss','price_target','gap_signal','bullish')
    qualified_stocks=[]
    for i in range(len(performance)):
        if performance[i]['sharpe']>=2 and performance[i]['ave_signal_ret']>=0.005 and performance[i]['num_signal']>20:
            qualified_stocks.append(performance[i])#qualified stocks stat, including('ticker','sharpe','ave_signal_ret','entry','stop_loss','price_target','gap_signal','bullish')
    print 'number of qualified stocks in '+str(year)+': %i' %len(qualified_stocks)
    return qualified_stocks

#%%portfolio performance

def portfolio(year,all_data,qualified_stocks,days):
    t_index=[]
    entry=[]
    stop_loss=[]
    price_target=[]
    multiplier=[]
    scenarios=[]
    for i in range(len(qualified_stocks)):
        temp=ticks.index(qualified_stocks[i][0])
        t_index.append(temp)
        entry.append(qualified_stocks[i]['entry'])
        stop_loss.append(qualified_stocks[i]['stop_loss'])
        price_target.append(qualified_stocks[i]['price_target'])
        multiplier.append(qualified_stocks[i]['multiplier'])
        scenarios.append(qualified_stocks[i]['scenario'])
    data=[]
    for i in range(len(all_data)):
        data.append(all_data[i].loc[year+'0101':year+'1231'])   
    asset=1
    ret=[]
    for t in range(1,len(data[ticks.index('YHOO')])):
        trade=False
        i=0
        while (not trade) and i<len(t_index):
            streak = data[t_index[i]].Streak[t-1]
            streakreturn = data[t_index[i]].StreakReturn[t-1]/data[t_index[i]].Streak[t-1]
            scenario = data[t_index[i]].Scenario[t-1]
            if qualified_stocks[i]['bullish']:
                if streak>=days and streakreturn>multiplier[i] and scenario==scenarios[i]:
                    if data[t_index[i]].High[t]>data[t_index[i]].Open[t]*(1+entry[i]):            
                        short_price = data[t_index[i]].Open[t]*(1+entry[i])
                        exit_target = data[t_index[i]].Close[t-1]*(1+price_target[i])
                        if data[t_index[i]].High[t]<short_price*(1+stop_loss[i]):
                            if data[t_index[i]].Low[t]<exit_target:
                                cover_price=exit_target
                            else:
                                cover_price=data[t_index[i]].Close[t]
                        else:
                            cover_price=short_price*(1+stop_loss[i])
                        asset= (2-(cover_price/short_price))*asset
                        ret.append(1-cover_price/short_price)
                        trade=True
            else:
                if streak<=-days and streakreturn>multiplier[i] and scenario==scenarios[i]:
                    if data[t_index[i]].Low[t]<data[t_index[i]].Open[t]*(1-entry[i]):
                        buy_price = data[t_index[i]].Open[t]*(1-entry[i])
                        exit_target = data[t_index[i]].Close[t-1]*(1-price_target[i])
                        if data[t_index[i]].Low[t]>buy_price*(1-stop_loss[i]):
                            if data[t_index[i]].High[t]>exit_target:
                                sell_price=exit_target
                            else:
                                sell_price=data[t_index[i]].Close[t]
                        else:
                            sell_price=buy_price*(1-stop_loss[i])
                        ret.append(sell_price/buy_price-1)
                        asset = (sell_price / buy_price)*asset
                        trade=True
            i+=1

    return [asset-1,sharpe(ret)]
#%%finding qualfied stocks  

def main(year,data,ticks,days):
    print 'for '+str(days)+' consec:'
    
    qualified_stocks=qs_list(year,data,ticks,days)
    
    l=portfolio(str(year),data,qualified_stocks,days)
    print 'Return in '+str(year)+' is %f'%l[0]
    print 'Sharpe in '+str(year)+' is %f'%l[1]
    
    return qualified_stocks

#%%insert corresponding year to get portfolio data and insert corresponding consecutive days desired.
#%%qualified stocks. True is bullish and false is bearish for the last parameter

qualified_stocks=main(2016,data,ticks,5)
for item in qualified_stocks: print item

for year in range(2011,2016):
    qualified_stocks=main(year,data,ticks,5)

qualified_stocks=main(2016,data,ticks,6)
for item in qualified_stocks: print item

for year in range(2011,2016):
    qualified_stocks=main(year,data,ticks,6)

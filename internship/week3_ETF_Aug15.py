'''
First of all please read all information in this file. Begin reading from function createParser and line 95
If you have some questions or some suggestions do not put on the back burner.

First step of all quantitive analyse is collecting data and  processing data. So this script shows how you can 
do this.

Andy & Sihan - downloading data and creating report. Index-wide
Guanqi & Court- calculating. Index-wide

'''

# script for gathering information
# for testing please use 10 tickers
# Attention! You must use only fixed paths! You must have posibility to use script from any folder! 
# Of course you can add new functions if you need but write comments (why you need tis function, what are parameters and what it returns)


import pandas as pd		# pandas can make your life easier
import numpy as np		# import if you like it
import argparse			# don't touch this

import os, sys, time		# maybe this libraries help you
from tqdm import tqdm		# great thing, make loops look cooler

from pandas.tseries.offsets import BDay
from pandas_datareader.data import DataReader

# for emails
from email.mime.multipart import MIMEMultipart
from email import encoders
from email.message import Message
from email.mime.base import MIMEBase
import smtplib
import mimetypes



# function catch scripts's parameters
# For example: python script.py -d True
# Returns arguments
def createParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--download', type=bool, default=False)
    return parser

def mailer(people):
    emailfrom = "Megatron.matvik@gmail.com"
    password = "rquqozfpharupslf"
    emailto = people
    today=time.strftime('%Y-%m-%d')
    # name of reports
    File = ['daily_global_report.csv', 'weekly_global_report.csv','Final_Reports/'+today+'Final_Report.csv']
    msg = MIMEMultipart()
    msg["From"] = emailfrom
    msg["To"] = emailto
    msg["Subject"] = "Reports for %s"%(time.strftime("%d/%m/%Y"))
    msg.preamble = "Three files attached"
    ctype, encoding = mimetypes.guess_type(File[0])
    if ctype is None or encoding is not None:
        ctype = "application/octet-stream"
    maintype, subtype = ctype.split("/", 1)
    for fl in File:
        fp = open(fl, "rb")
        attachment = MIMEBase(maintype, subtype)
        attachment.set_payload(fp.read())
        fp.close()
        encoders.encode_base64(attachment)
        attachment.add_header("Content-Disposition", "attachment", filename=fl)
        msg.attach(attachment)
    server = smtplib.SMTP("smtp.gmail.com:587")
    server.starttls()
    server.login(emailfrom,password)
    server.sendmail(emailfrom, emailto, msg.as_string())
    server.quit()
    print 'Message sent'
    

def Download_all_data(ticker):
    # your code
    all_data=DataReader(ticker,'yahoo',start='20080101')
    return all_data     # your final goal in this function

    
def Download_last_row_data(start_date,ticker):
    # your code
    try:
        last_row=DataReader(ticker,'yahoo',start=start_date)
    except IOError:
        return pd.DataFrame()
    return last_row     # your final goal in this function

	


#%%	
def Calculate(data, download):
    # your code must have columns:
    # RSI
    # ADX
    # Devidends
    # P/E
    # Accumulation/Distribution Index 
    # Chaikin Oscillator
    # Scenarios (from 0 to 9, you've done it previously)
    # 1,2,5,10 days change
    # 1,2,3,6 months change (20 days, 40 days, 60 days, 125 days)
    # annual change (250 days)
    # Beta
    # 1,3 months volume
    # moving averages for 5,20,50,100,200 days
    # 1,3,6 months and 1,5 years intra day volatility (Close-Open)
    # 1,3,6 months and 1,5 years intra day volatility (Close-Close)
    # 1,3,6 months and 1,5 years intra day volatility (High-Low)
    # 52-week high
    # 52-week low
    # Cross can be NoCross/Bullish/Bearish
    #	if MA50(T-1) < MA200(T-1) and MA200(T) < MA50(T) - Bullish
    #	if MA50(T-1) > MA200(T-1) and MA200(T) > MA50(T) - Bearish
    # Trend can be NoTrend/Bullsih/Bearish
    #	if daily gain >= 2% and daily gain <= 4% - Bullish
    #	if daily gain <= -2% and daily gain >= -4% - Bearish
    # Count how many days up/down:
    # 		if up/down above 5 days ---> Bullish/Bearish
    #			Count the percent CHG of consecutive upside/downside
    #		else ---> empty
    # Percent change can be 3-4,4-5,5-6,6-7,7-8,8-9,9-10
    #	if abs(daily gain) >= 3% and abs(daily gain) <= 4% - 3%-4%
    #	else if abs(daily gain) >= 4% and abs(daily gain) <= 5% - 4%-5%
    # 	...
    # Where stock goes up/down ONLY if percent change more than 3% and less than 10%. Check "Percent change"
    # 

# data is a frame that you play around through all this function

    if download == True:
        data['MA5']=data['Adj Close'].rolling(window=5).mean()
        data['MA20']=data['Adj Close'].rolling(window=20).mean()
        data['MA50']=data['Adj Close'].rolling(window=50).mean()
        data['MA100']=data['Adj Close'].rolling(window=100).mean()
        data['MA200']=data['Adj Close'].rolling(window=200).mean()
        data['Change1d']=(data['Adj Close']-data['Adj Close'].shift(1))/data['Adj Close'].shift(1)
        data['Change2d']=(data['Adj Close']-data['Adj Close'].shift(2))/data['Adj Close'].shift(2)
        data['Change5d']=(data['Adj Close']-data['Adj Close'].shift(5))/data['Adj Close'].shift(5)
        data['Change10d']=(data['Adj Close']-data['Adj Close'].shift(10))/data['Adj Close'].shift(10)
        data['Change20d']=(data['Adj Close']-data['Adj Close'].shift(20))/data['Adj Close'].shift(20)
        data['Change40d']=(data['Adj Close']-data['Adj Close'].shift(40))/data['Adj Close'].shift(40)
        data['Change60d']=(data['Adj Close']-data['Adj Close'].shift(60))/data['Adj Close'].shift(60)
        data['Change125d']=(data['Adj Close']-data['Adj Close'].shift(125))/data['Adj Close'].shift(125)
        data['Change250d']=(data['Adj Close']-data['Adj Close'].shift(250))/data['Adj Close'].shift(250)
        data['Volume20d']=data['Volume'].rolling(window=20).mean()
        data['Volume60d']=data['Volume'].rolling(window=60).mean()
        data['Vol_20d_Close-Open']=((data['Close']-data['Open'])/data['Open']).rolling(window=20).std()*np.sqrt(250/20)
        data['Vol_60d_Close-Open']=((data['Close']-data['Open'])/data['Open']).rolling(window=60).std()*np.sqrt(250/60)
        data['Vol_125d_Close-Open']=((data['Close']-data['Open'])/data['Open']).rolling(window=125).std()*np.sqrt(250/125)
        data['Vol_250d_Close-Open']=((data['Close']-data['Open'])/data['Open']).rolling(window=250).std()
        data['Vol_1250d_Close-Open']=((data['Close']-data['Open'])/data['Open']).rolling(window=1250).std()*np.sqrt(0.2)
        data['Vol_20d_Close-Close']=data['Change1d'].rolling(window=20).std()*np.sqrt(250/20)
        data['Vol_60d_Close-Close']=data['Change1d'].rolling(window=60).std()*np.sqrt(250/60)
        data['Vol_125d_Close-Close']=data['Change1d'].rolling(window=125).std()*np.sqrt(250/125)
        data['Vol_250d_Close-Close']=data['Change1d'].rolling(window=250).std()
        data['Vol_1250d_Close-Close']=data['Change1d'].rolling(window=1250).std()*np.sqrt(0.2)
        data['Vol_20d_High-Low']=((data['High']-data['Low'])/data['Low']).rolling(window=20).std()*np.sqrt(250/20)
        data['Vol_60d_High-Low']=((data['High']-data['Low'])/data['Low']).rolling(window=60).std()*np.sqrt(250/60)
        data['Vol_125d_High-Low']=((data['High']-data['Low'])/data['Low']).rolling(window=125).std()*np.sqrt(250/125)
        data['Vol_250d_High-Low']=((data['High']-data['Low'])/data['Low']).rolling(window=250).std()
        data['Vol_1250d_High-Low']=((data['High']-data['Low'])/data['Low']).rolling(window=1250).std()*np.sqrt(0.2)
        data['High_52W']=data['High'].rolling(window=250).max()
        data['Low_52W']=data['Low'].rolling(window=250).min()
        data['Gap']=data['Open']/(data['Close'].shift(1))-1
        data['Beta']=SP500['Return'].rolling(window=252).cov(data['Change1d'])/SP500['Return'].rolling(window=252).var()
        
        data['Days_up/down']=pd.Series()
        data.ix[0,'Days_up/down']=0
        for i in range(1,len(data['Days_up/down'])):
            if data.ix[i,'Change1d']>0:
                if data.ix[i-1,'Days_up/down']>=0:
                    data.ix[i,'Days_up/down']=data.ix[i-1,'Days_up/down']+1
                else:
                    data.ix[i,'Days_up/down']=1
            if data.ix[i,'Change1d']==0:
                if data.ix[i-1,'Days_up/down']>=0:
                    data.ix[i,'Days_up/down']=data.ix[i-1,'Days_up/down']+1
                else:
                    data.ix[i,'Days_up/down']=data.ix[i-1,'Days_up/down']-1
            if data.ix[i,'Change1d']<0:
                if data.ix[i-1,'Days_up/down']>=0:
                    data.ix[i,'Days_up/down']=-1
                else:
                    data.ix[i,'Days_up/down']=data.ix[i-1,'Days_up/down']-1
        
        temp=pd.DataFrame([data['MA50']>data['MA200'],data['MA50'].shift(1)<data['MA200'].shift(1)])
        data.loc[temp.all(),'Cross']='Bullish'
        temp=pd.DataFrame([data['MA50']<data['MA200'],data['MA50'].shift(1)>data['MA200'].shift(1)])
        data.loc[temp.all(),'Cross']='Bearish'
        
        data['ADL']=pd.Series()
        data.ix[0,'ADL']=0
        for i in range(1,len(data['ADL'])):
            data.ix[i,'ADL']=data.ix[i-1,'ADL']+((data.ix[i,'Close']-data.ix[i,'Low'])-(data.ix[i,'High']-data.ix[i,'Close']))/(data.ix[i,'High']-data.ix[i,'Low'])*data.ix[i,'Volume']
        data['Chaikin']=data['ADL'].ewm(span=3).mean()-data['ADL'].ewm(span=10).mean()
        
        dividends=DataReader(ticker,'yahoo-actions',start='20110101')
        try:
            data['Dividend']=dividends.loc[dividends['action']=='DIVIDEND']['value']
        except:
            data['Dividend']=pd.Series()
        data['Dividend']=data['Dividend'].fillna(method='ffill')

        data['Dividend_Yield']=data['Dividend']/data['Adj Close']*4
            
        
        diff=data['Adj Close'].diff()
        up=diff.copy()
        up[up<0]=0
        down=-diff.copy()
        down[down<0]=0
        data['RSI']=100-100/(1+up.rolling(window=14).mean()/down.rolling(window=14).mean())
        

        
        
        #ADX & Scenarios
        high = data.High
        low = data.Low
        close = data.Close
        rsi = data.RSI
        index = 0
        TR14 = []
        PDM14 = []
        MDM14 = []
        PDI14 = []
        MDI14 = []
        DX= []
        ADX = []
        Scenario = []
        for i in range(14): 
            TR14.append(0), PDM14.append(0),MDM14.append(0),PDI14.append(0),
            MDI14.append(0), DX.append(0), ADX.append("-")
        for i in range(13): ADX.append("-")
        TR=[0]
        #+DM and -DM 
        PDM =[0]
        MDM =[0]
        for day in high:
            if index>0:
                TR.append(max([high[index]-low[index],abs(high[index]-close[index-1]),abs(low[index]-close[index-1])]))
                if high[index]-high[index-1]>low[index-1]-low[index] and high[index]-high[index-1]>0:
                    PDM.append(high[index]-high[index-1])
                else: PDM.append(0)
                if high[index]-high[index-1]<low[index-1]-low[index] and low[index-1]-low[index]>0:
                    MDM.append(low[index-1]-low[index])
                else: MDM.append(0)
            if index==14: 
                TR14.append(sum(TR)),MDM14.append(sum(MDM)),PDM14.append(sum(PDM))
                PDI14.append(PDM14[index]/TR14[index]*100)
                MDI14.append(MDM14[index]/TR14[index]*100)
                DX.append(abs(PDI14[index]-MDI14[index])/(PDI14[index]+MDI14[index])*100)
            if index >14: 
                TR14.append(TR14[index-1]-TR14[index-1]/14+TR[index])
                MDM14.append(MDM14[index-1]-MDM14[index-1]/14+MDM[index])
                PDM14.append(PDM14[index-1]-PDM14[index-1]/14+PDM[index])
                PDI14.append(PDM14[index]/TR14[index]*100)
                MDI14.append(MDM14[index]/TR14[index]*100)
                DX.append(abs(PDI14[index]-MDI14[index])/(PDI14[index]+MDI14[index])*100)
            if index==27: ADX.append(sum(DX)/14)
            if index>27:  ADX.append((13*ADX[index-1]+DX[index])/14)
            if ADX[index] <20:
                if rsi[index] > 70:
                    Scenario.append(2)
                elif rsi[index] < 30:
                    Scenario.append(8)
                else:
                    Scenario.append(5)
            elif ADX[index]>25 and ADX[index]!='-' and PDI14[index]>MDI14[index]:
                if rsi[index] > 70:
                    Scenario.append(1)
                elif rsi[index] < 30:
                    Scenario.append(7)
                else:
                    Scenario.append(4)
            elif ADX[index]>25 and ADX[index]!='-' and PDI14[index]<MDI14[index]:
                if rsi[index] > 70:
                    Scenario.append(3)
                elif rsi[index] < 30:
                    Scenario.append(9)
                else:
                    Scenario.append(6)
            else: Scenario.append(0)
            index +=1
        
        if len(high)<27:
                ADX =[]
                for i in range(len(high)):
                    ADX.append("-")    
        data['TR14']=TR14
        data['PDM14']=PDM14
        data['MDM14']=MDM14
        data['ADX']= ADX
        data['Scenario']=Scenario
        #end of ADX & Scenario
        
        
        
        data.to_csv(path_or_buf='Data/'+ticker+'.csv')		# your final goal in this function
    elif download == False:
        #data=data
        try:
            start_index=data.index.get_loc(start_date.strftime('%Y-%m-%d'))
        except:
            start_index=len(data.index)
        #index=data.index.get_values()
        for i in range(start_index,len(data.index)):
            data.ix[i,'MA5']=np.nansum(data['Adj Close'][i-5:i])/5
            data.ix[i,'MA20']=np.nansum(data['Adj Close'][i-20:i])/20
            data.ix[i,'MA50']=np.nansum(data['Adj Close'][i-50:i])/50
            data.ix[i,'MA100']=np.nansum(data['Adj Close'][i-100:i])/100
            data.ix[i,'MA200']=np.nansum(data['Adj Close'][i-200:i])/200
            data.ix[i,'Change1d']=  (data['Adj Close'][i]-data['Adj Close'][i-1])/data['Adj Close'][i-1]
            data.ix[i,'Change2d']=  (data['Adj Close'][i]-data['Adj Close'][i-2])/data['Adj Close'][i-2]
            data.ix[i,'Change5d']=  (data['Adj Close'][i]-data['Adj Close'][i-5])/data['Adj Close'][i-5]
            data.ix[i,'Change10d']= (data['Adj Close'][i]-data['Adj Close'][i-10])/data['Adj Close'][i-10]
            data.ix[i,'Change20d']= (data['Adj Close'][i]-data['Adj Close'][i-20])/data['Adj Close'][i-20]
            data.ix[i,'Change40d']= (data['Adj Close'][i]-data['Adj Close'][i-40])/data['Adj Close'][i-40]
            data.ix[i,'Change60d']= (data['Adj Close'][i]-data['Adj Close'][i-60])/data['Adj Close'][i-60]
            data.ix[i,'Change125d']=(data['Adj Close'][i]-data['Adj Close'][i-125])/data['Adj Close'][i-125]
            data.ix[i,'Change250d']=(data['Adj Close'][i]-data['Adj Close'][i-250])/data['Adj Close'][i-250]
            data.ix[i,'Volume20d']=np.nansum(data['Volume'][i-20:i])/20
            data.ix[i,'Volume60d']=np.nansum(data['Volume'][i-60:i])/60
            data.ix[i,'Vol_20d_Close-Open']=  np.nanstd((data['Close'][i-20:i]   -data['Open'][i-20:i]  )/data['Open'][i-20:i]  ) *np.sqrt(250/20)
            data.ix[i,'Vol_60d_Close-Open']=  np.nanstd((data['Close'][i-60:i]   -data['Open'][i-60:i]  )/data['Open'][i-60:i]  ) *np.sqrt(250/60)
            data.ix[i,'Vol_125d_Close-Open']= np.nanstd((data['Close'][i-125:i]  -data['Open'][i-125:i] )/data['Open'][i-125:i] ) *np.sqrt(2)
            data.ix[i,'Vol_250d_Close-Open']= np.nanstd((data['Close'][i-250:i]  -data['Open'][i-250:i] )/data['Open'][i-250:i] )
            data.ix[i,'Vol_1250d_Close-Open']=np.nanstd((data['Close'][i-1250:i] -data['Open'][i-1250:i])/data['Open'][i-1250:i]) *np.sqrt(0.2)
            data.ix[i,'Vol_20d_Close-Close']=  np.nanstd(data['Change1d'][i-20:i])*np.sqrt(250/20)
            data.ix[i,'Vol_60d_Close-Close']=  np.nanstd(data['Change1d'][i-60:i])*np.sqrt(250/60)
            data.ix[i,'Vol_125d_Close-Close']= np.nanstd(data['Change1d'][i-125:i])*np.sqrt(2)
            data.ix[i,'Vol_250d_Close-Close']= np.nanstd(data['Change1d'][i-250:i])
            data.ix[i,'Vol_1250d_Close-Close']=np.nanstd(data['Change1d'][i-1250:i])*np.sqrt(0.2)
            data.ix[i,'Vol_20d_High-Low']=  np.nanstd((data['High'][i-20:i]  -data['Low'][i-20:i]  )/data['Low'][i-20:i]  )*np.sqrt(250/20)
            data.ix[i,'Vol_60d_High-Low']=  np.nanstd((data['High'][i-60:i]  -data['Low'][i-60:i]  )/data['Low'][i-60:i]  )*np.sqrt(250/60)
            data.ix[i,'Vol_125d_High-Low']= np.nanstd((data['High'][i-125:i] -data['Low'][i-125:i] )/data['Low'][i-125:i] )*np.sqrt(2)            
            data.ix[i,'Vol_250d_High-Low']= np.nanstd((data['High'][i-250:i] -data['Low'][i-250:i] )/data['Low'][i-250:i] )
            data.ix[i,'Vol_1250d_High-Low']=np.nanstd((data['High'][i-1250:i]-data['Low'][i-1250:i])/data['Low'][i-1250:i])*np.sqrt(0.2)
            data.ix[i,'Low_52W']=data['Low'][i-250:i].min()
            data.ix[i,'High_52W']=data['High'][i-250:i].max()
            data.ix[i,'Gap']=data['Close'][i]/data['Open'][i-1]-1
            data.ix[i,'Beta']=SP500.ix[i-251:i+1,'Return'].cov(data.ix[i-251:i+1,'Change1d'])/SP500.ix[i-251:i+1,'Return'].var()
            
            if data.ix[i,'Change1d']>0:
                if data.ix[i-1,'Days_up/down']>=0:
                    data.ix[i,'Days_up/down']=data.ix[i-1,'Days_up/down']+1
                else:
                    data.ix[i,'Days_up/down']=1
            if data.ix[i,'Change1d']==0:
                if data.ix[i-1,'Days_up/down']>=0:
                    data.ix[i,'Days_up/down']=data.ix[i-1,'Days_up/down']+1
                else:
                    data.ix[i,'Days_up/down']=data.ix[i-1,'Days_up/down']-1
            if data.ix[i,'Change1d']<0:
                if data.ix[i-1,'Days_up/down']>=0:
                    data.ix[i,'Days_up/down']=-1
                else:
                    data.ix[i,'Days_up/down']=data.ix[i-1,'Days_up/down']-1
        
            
            if data.ix[i,'MA50']>data.ix[i,'MA200'] and data.ix[i-1,'MA50']<data.ix[i-1,'MA200']:
                data.ix[i,'Cross']='Bullish'
            if data.ix[i,'MA50']<data.ix[i,'MA200'] and data.ix[i-1,'MA50']>data.ix[i-1,'MA200']:
                data.ix[i,'Cross']='Bearish'
            
            data.ix[i,'ADL']=data.ix[i-1,'ADL']+((data.ix[i,'Close']-data.ix[i,'Low'])-(data.ix[i,'High']-data.ix[i,'Close']))/(data.ix[i,'High']-data.ix[i,'Low'])*data.ix[i,'Volume']
            data.ix[i,'Chaikin']=(data.ix[i-1,'Chaikin']+data.ix[i,'ADL'])/2-(data.ix[i-1,'Chaikin']*9+data.ix[i,'ADL'])/11
            
            dividends=DataReader(ticker,'yahoo-actions',start='20110101')
            try:
                data['Dividend']=dividends.loc[dividends['action']=='DIVIDEND']['value']
            except:
                data['Dividend']=pd.Series()
            data['Dividend']=data['Dividend'].fillna(method='ffill')

            data['Dividend_Yield']=data['Dividend']/data['Adj Close']*4
            #RSI    
            diff=data['Adj Close'][i-15:i].diff()
            up=diff.copy()
            up[up<0]=0.0
            down=-diff.copy()
            down[down<0]=0.0
            data.ix[i,'RSI']=100.0-100.0/(1.0+np.nansum(up)/np.nansum(down))
            
            #ADX
            data['ADX']=pd.to_numeric(data['ADX'],errors='ignore')
            TR=max(data.ix[i,'High']-data.ix[i,'Low'],abs(data.ix[i,'High']-data.ix[i,'Close']),abs(data.ix[i,'Low']-data.ix[i,'Close']))
            PDM=0
            MDM=0
            if data.ix[i,'High']-data.ix[i,'High']>data.ix[i,'Low']-data.ix[i,'Low']:
                PDM=max(data.ix[i,'High']-data.ix[i,'High'],0)
            else:
                MDM=max(data.ix[i,'Low']-data.ix[i,'Low'],0)
            data.ix[i,'TR14']= data.ix[i-1,'TR14']- data.ix[i-1,'TR14']/14+TR
            data.ix[i,'PDM14']=data.ix[i-1,'PDM14']-data.ix[i-1,'PDM14']/14+PDM
            data.ix[i,'MDM14']=data.ix[i-1,'MDM14']-data.ix[i-1,'MDM14']/14+MDM
            PDI14=data['PDM14'][i]/data['TR14'][i]*100
            MDI14=data['MDM14'][i]/data['TR14'][i]*100
            DI14Diff=abs(PDI14-MDI14)
            DI14Sum=PDI14+MDI14
            DX14=DI14Diff/DI14Sum*100
            data.ix[i,'ADX']=(float(data.ix[i-1,'ADX'])*13+DX14)/14
            
            #scenario
            data.ix[i,'Scenario']=0
            if data['ADX'][i]<20:
                data.ix[i,'Scenario']=5
            if data['ADX'][i]>25 and PDI14>MDI14:
                data.ix[i,'Scenario']=4
            if data['ADX'][i]>25 and PDI14<MDI14:
                data.ix[i,'Scenario']=6
            if data['ADX'][i]<20 and data['RSI'][i]>=70:
                data.ix[i,'Scenario']=2
            if data['ADX'][i]>25 and PDI14>MDI14 and data['RSI'][i]>=70:
                data.ix[i,'Scenario']=1
            if data['ADX'][i]>25 and PDI14<MDI14 and data['RSI'][i]>=70:
                data.ix[i,'Scenario']=3
            if data['ADX'][i]<20 and data['RSI'][i]<=30:
                data.ix[i,'Scenario']=8
            if data['ADX'][i]>25 and PDI14>MDI14 and data['RSI'][i]<=30:
                data.ix[i,'Scenario']=7
            if data['ADX'][i]>25 and PDI14<MDI14 and data['RSI'][i]<=30:
                data.ix[i,'Scenario']=9
            
            

            
        data.to_csv(path_or_buf='Data/'+ticker+'.csv')
            
#%%
	
def Reporter(year,mode_of_processing):
    # your code for connection last date in ticker.csv and final_report
    tickers=[]
    with open('XLF.txt') as f:
        for ticker in f.readlines():
            tickers.append(ticker[:-1])
    
    if mode_of_processing:
        index=pd.read_csv('SP500.csv',index_col=0).loc[str(year)+'-01-01':str(year)+'-12-31'].index
    else:
        temp=pd.read_csv('SP500.csv',index_col=0)
        try:
            start_index=temp.index.get_loc(start_date.strftime('%Y-%m-%d'))
            index=temp.index[start_index:]
        except:
            index=[]
        
        
    for date in tqdm(index):
        #date=date.strftime('%Y-%m-%d')
        if not os.path.isfile('Final_Reports/'+date+'Final_Report.csv'):
            global_data=pd.DataFrame()
            for ticker in tickers:
                try:
                    temp=pd.read_csv('Data/'+ticker+'.csv',index_col=0).loc[date:date]
                    temp.index=[ticker]
                    global_data=pd.concat([global_data,temp])
                    
                except: continue
            
            global_data=global_data.drop(global_data[global_data['Volume']==0].index)
            global_data.to_csv('Final_Reports/'+date+'Final_Report.csv')

    #return final_report						# your final goal in this function
#%%

def Index_Wide(mode_of_processing):
    # put your code on strategy 11 and 12. 
    # Attention! You don't need to calculate, just open files, all data is calculated in function "Calculate"
    # Check file "Brooklyn Risk Advisors_Coding Exercise_Index-Wide Strategies.docx"

    if mode_of_processing:
        index=pd.read_csv('SP500.csv',index_col=0).index
    else:
        temp=pd.read_csv('SP500.csv',index_col=0)
        try:
            start_index=temp.index.get_loc(start_date.strftime('%Y-%m-%d'))
            index=temp.index[start_index:]
        except:
            index=[]
        
    daily_data=pd.DataFrame()
    for date in tqdm(index):
        #date=date.strftime('%Y-%m-%d')
        global_data=pd.read_csv('Final_Reports/'+date+'Final_Report.csv',index_col=0)

        
        num=len(global_data['Open'])
        fnum=float(num)
        daily_data.loc[date,'Bullish_Gap_1%-2%']=len(global_data['Gap'][pd.DataFrame([global_data['Gap']>=0.01, global_data['Gap']<0.02]).all()])/  fnum
        daily_data.loc[date,'Bullish_Gap_2%-3%']=len(global_data['Gap'][pd.DataFrame([global_data['Gap']>=0.02, global_data['Gap']<0.03]).all()])/  fnum
        daily_data.loc[date,'Bullish_Gap_3%-4%']=len(global_data['Gap'][pd.DataFrame([global_data['Gap']>=0.03, global_data['Gap']<0.04]).all()])/  fnum
        daily_data.loc[date,'Bearish_Gap_1%-2%']=len(global_data['Gap'][pd.DataFrame([global_data['Gap']<=-0.01, global_data['Gap']>-0.02]).all()])/fnum
        daily_data.loc[date,'Bearish_Gap_2%-3%']=len(global_data['Gap'][pd.DataFrame([global_data['Gap']<=-0.02, global_data['Gap']>-0.03]).all()])/fnum
        daily_data.loc[date,'Bearish_Gap_3%-4%']=len(global_data['Gap'][pd.DataFrame([global_data['Gap']<=-0.03, global_data['Gap']>-0.04]).all()])/fnum
        daily_data.loc[date,'Low_52W']=len(global_data['Low_52W'][global_data['Low_52W']==global_data['Low']])/    fnum
        daily_data.loc[date,'High_52W']=len(global_data['High_52W'][global_data['High_52W']==global_data['High']])/fnum
        daily_data.loc[date,'Scenario_1']=len(global_data['Scenario'][global_data['Scenario']==1])/fnum
        daily_data.loc[date,'Scenario_2']=len(global_data['Scenario'][global_data['Scenario']==2])/fnum
        daily_data.loc[date,'Scenario_3']=len(global_data['Scenario'][global_data['Scenario']==3])/fnum
        daily_data.loc[date,'Scenario_4']=len(global_data['Scenario'][global_data['Scenario']==4])/fnum
        daily_data.loc[date,'Scenario_5']=len(global_data['Scenario'][global_data['Scenario']==5])/fnum
        daily_data.loc[date,'Scenario_6']=len(global_data['Scenario'][global_data['Scenario']==6])/fnum
        daily_data.loc[date,'Scenario_7']=len(global_data['Scenario'][global_data['Scenario']==7])/fnum
        daily_data.loc[date,'Scenario_8']=len(global_data['Scenario'][global_data['Scenario']==8])/fnum
        daily_data.loc[date,'Scenario_9']=len(global_data['Scenario'][global_data['Scenario']==9])/fnum
        daily_data.loc[date,'RSI>70']=len(global_data['RSI'][global_data['RSI']>70])/fnum
        daily_data.loc[date,'RSI<30']=len(global_data['RSI'][global_data['RSI']<30])/fnum
        daily_data.loc[date,'Scenario5&vol1M']=len(global_data['Scenario'][pd.DataFrame([global_data['Scenario']==5,global_data['Volume']>=2*global_data['Volume20d']]).all()])/fnum
        daily_data.loc[date,'Scenario5&vol3M']=len(global_data['Scenario'][pd.DataFrame([global_data['Scenario']==5,global_data['Volume']>=2*global_data['Volume60d']]).all()])/fnum
        daily_data.loc[date,'AboveMA5']=       len(global_data['Adj Close'][global_data['Adj Close']>global_data['MA5']]                                                      )/fnum
        daily_data.loc[date,'AboveMA20']=      len(global_data['Adj Close'][global_data['Adj Close']>global_data['MA20']]                                                     )/fnum
        daily_data.loc[date,'AboveMA50']=      len(global_data['Adj Close'][global_data['Adj Close']>global_data['MA50']]                                                     )/fnum
        daily_data.loc[date,'AboveMA100']=     len(global_data['Adj Close'][global_data['Adj Close']>global_data['MA100']]                                                    )/fnum
        daily_data.loc[date,'AboveMA200']=     len(global_data['Adj Close'][global_data['Adj Close']>global_data['MA200']]                                                    )/fnum
        daily_data.loc[date,'up_1%-2%']=       len(global_data['Change1d'][pd.DataFrame([global_data['Change1d']>=0.01,global_data['Change1d']<0.02]).all()]                  )/fnum
        daily_data.loc[date,'up_2%-3%']=       len(global_data['Change1d'][pd.DataFrame([global_data['Change1d']>=0.02,global_data['Change1d']<0.03]).all()]                  )/fnum
        daily_data.loc[date,'up_3%-4%']=       len(global_data['Change1d'][pd.DataFrame([global_data['Change1d']>=0.03,global_data['Change1d']<0.04]).all()]                  )/fnum
        daily_data.loc[date,'up>4%']=          len(global_data['Change1d'][global_data['Change1d']>=0.04]                                                                     )/fnum
        daily_data.loc[date,'down_1%-2%']=     len(global_data['Change1d'][pd.DataFrame([global_data['Change1d']>-0.02,global_data['Change1d']<=-0.01]).all()]                )/fnum
        daily_data.loc[date,'down_2%-3%']=     len(global_data['Change1d'][pd.DataFrame([global_data['Change1d']>-0.03,global_data['Change1d']<=-0.02]).all()]                )/fnum
        daily_data.loc[date,'down_3%-4%']=     len(global_data['Change1d'][pd.DataFrame([global_data['Change1d']>-0.04,global_data['Change1d']<=-0.03]).all()]                )/fnum
        daily_data.loc[date,'down>4%']=        len(global_data['Change1d'][global_data['Change1d']<-0.04]                                                                     )/fnum
        daily_data.loc[date,'X1.5_Vol3M']=     len(global_data['Volume'][global_data['Volume']>=1.5*global_data['Volume60d']]                                                 )/fnum
        daily_data.loc[date,'X2_Vol3M']=       len(global_data['Volume'][global_data['Volume']>=2*global_data['Volume60d']]                                                   )/fnum
        daily_data.loc[date,'X1.5_Vol1M']=     len(global_data['Volume'][global_data['Volume']>=1.5*global_data['Volume20d']]                                                 )/fnum
        daily_data.loc[date,'X2_Vol1M']=       len(global_data['Volume'][global_data['Volume']>=2*global_data['Volume20d']]                                                   )/fnum
        daily_data.loc[date,'Cumulative_Dollar_up_Volume']=np.nansum(global_data['Volume'][global_data['Change1d']>0]*global_data['Adj Close'][global_data['Change1d']>0])/fnum
        daily_data.loc[date,'Cumulative_Dollar_down_Volume']=np.nansum(global_data['Volume'][global_data['Change1d']<0]*global_data['Adj Close'][global_data['Change1d']<0])/fnum
        daily_data.loc[date,'Ave_Dollar_Volume']= np.nansum(global_data['Volume']*global_data['Adj Close'])/fnum       
        daily_data.loc[date,'ADL>0']=    len(global_data['ADL'][global_data['ADL']>0]        )/fnum
        daily_data.loc[date,'ADL<0']=    len(global_data['ADL'][global_data['ADL']<0]        )/fnum
        daily_data.loc[date,'Chaikin>0']=len(global_data['Chaikin'][global_data['Chaikin']>0])/fnum
        daily_data.loc[date,'Chaikin<0']=len(global_data['Chaikin'][global_data['Chaikin']<0])/fnum
        daily_data.loc[date,'#of companies']=num
        
    daily_data.index=pd.to_datetime(daily_data.index)
    weekly_data=daily_data.asfreq('W-FRI',method='pad')
            
    
    return daily_data, weekly_data		# where data is pandas.DataFrame()
	
#%%	
# main statement, execute first of all
# you can navigate you script throgh this if statement	
if __name__ == '__main__':
    parser = createParser()
    namespace = parser.parse_args(sys.argv[1:])
    # download means that:
    # 1) if True 
    #		a) download ALL data#		b) calculate ALL data and SAVE .csv file to folder 'Data'
    #		c) create report and save it to folder with this script
    # 2) if False
    #		a) download ONLY LAST day data 
    #		b) calculate ONLY LAST day data and connect it with .csv file in folder 'Data'
    #		c) create report and save it to folder with this script 
    
    download = namespace.download
    download=True
    
    this_year=pd.datetime.today().year
    
    # we need finctions:
    # 1) Download_all_data(ticker) - download data from yahoo/google/etc
    #		Returns  pandas.DataFrame()
    # 2) Download_last_row_data(ticker) - download ONLY last row from yahoo/google/etc. It makes script faster
    #		Returns  pandas.DataFrame(ticker)
    # 3) Calculate(data, download) - calculate data. Parameter 'data' = pandas.DataFrame from 1) or 2).
    #		a) Open .csv file from folder 'Data' if it exists.
    #		b) Calculate data
    #		c) Connect new row to .csv file from folder 'Data' if it exists OR save it
    # 4) Reporter(final_report, ticker) - takes last row from file, get all information for tomorrow and connect it to final_report.DataFrame()
    # 5) Index_wide() - strategy 11 and 12. Collect global information about all stocks in percents. 
    # For example: 15% of all stocks has 1-2% CHG
    final_report = pd.DataFrame()		# add all columns that you need
										# pd.DataFrame(columns=('1','2','3'...))
										# one column must show S&P500, Russell1000, Russell2000 and etc
										# for testing you can use 2 files with 5 tickers in each. First file S&P500 other is Russell1000
    global_report = pd.DataFrame()		# 11 - 12 strategies. Index-Wide strategies
    # in loops you can use threadsif you want to improve speed of calculating
    
    SP500=DataReader('^GSPC','yahoo',start='20080101')
    SP500['Return']=SP500['Adj Close']/SP500['Adj Close'].shift(1)-1
    SP500.to_csv('SP500.csv')
    spy_index=SP500.index
    
    if download == True:
        for year in range(2008,this_year+1):
            with open('XLF.txt') as tickers:
                for ticker in tqdm(tickers.readlines()):
                    ticker = ticker[:-1]	# delete last symbol '\n'
                    if not os.path.isfile('Data/'+ticker+'.csv'):
                        try: Calculate(Download_all_data(ticker), download)
                        except: 
                            #print ticker 
                            continue
                Reporter(year,download)
        daily_global_report, weekly_global_report = Index_Wide(download)
        daily_global_report.to_csv('daily_global_report.csv')
        weekly_global_report.to_csv('weekly_global_report.csv')
                        #  first report
                    #final_report = Reporter(final_report, ticker)
                #today=pd.datetime.today().strftime('%Y-%m-%d')    
                #final_report.to_csv(today+'final_report.csv')
                #Reporter(download)
	   		# daily and weekly global report
	   		# download shows, we need to make new report or uprgrade all
	   		# upgrade means that we check new line(today) and add it to old
    else:
        with open('XLF.txt') as tickers:
            for ticker in tqdm(tickers.readlines()):
                ticker = ticker[:-1]	# delete last symbol '\n'
                
                try:
                    old_data=pd.read_csv('Data/'+ticker+'.csv',index_col=0)
                    old_data.index=pd.to_datetime(old_data.index)
                    columns=old_data.columns
                    start_date=pd.to_datetime(old_data.index[-1])+BDay(1)
                    data=Download_last_row_data(start_date,ticker)

                    frames = [old_data,data]
                    data=pd.concat(frames)
                    data=data.reindex_axis(columns,axis=1)
                    Calculate(data, download)
                    
                except IOError:
                    pass
            Reporter(this_year,download)
            # daily and weekly global report
            # download shows, we need to make new report or uprgrade all
            # upgrade means that we check new line(today) and add it to old
            daily_global_report, weekly_global_report = Index_Wide(download)
            old_daily_global_report = pd.read_csv('daily_global_report.csv',index_col=0)
            old_daily_global_report.index=pd.to_datetime(old_daily_global_report.index)
            old_weekly_global_report = pd.read_csv('weekly_global_report.csv',index_col=0)
            old_weekly_global_report.index=pd.to_datetime(old_weekly_global_report.index)
            
            daily_frames = [old_daily_global_report, daily_global_report]
            weekly_frames = [old_weekly_global_report, weekly_global_report]
            
            daily_global_report = pd.concat(daily_frames)
            weekly_global_report = pd.concat(weekly_frames)
            
            daily_global_report.to_csv('daily_global_report.csv')
            weekly_global_report.to_csv('weekly_global_report.csv')
#%%	   
    # Email created reports
    #people = ['andrew.c.haworth@vanderbilt.edu']#,'Megatron.matvik@gmail.com'] # list of emails
    #for person in people:
     #   mailer(person)
	# finally save final_report.csv, weekly_global_report.csv and daily_global_report to folder with this script
                

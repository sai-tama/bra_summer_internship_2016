# -*- coding: utf-8 -*-
"""
Created on Thu Jun 23 17:24:46 2016

@author: renguanqi
"""
#%%
import csv
import threading
import Queue
import numpy as np
import math


from pandas.io.data import DataReader
from itertools import product

#%% 
#download data

with open('constituents.csv', 'rb') as f:
    reader = csv.reader(f)
    tickers=list(reader)
ticks=[]
ts=[]
for i in range(0,504):
    ts.extend(tickers[i])

start="2006/1/1"

exitFlag = 0

data=[]

class myThread (threading.Thread):
    def __init__(self, threadID, name, q):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.q = q
    def run(self):
        print "Starting " + self.name
        process_data(self.name, self.q, ticks)
        queueLock.acquire()        
        print "Exiting " + self.name
        queueLock.release()

def process_data(threadName, q,ticks):
    while not exitFlag:
        queueLock.acquire()
        if not workQueue.empty():
            t = q.get()
            print threadName+" downloading data of %s" % t
            queueLock.release()
            temp=DataReader(t,"yahoo",start)
            queueLock.acquire()
            data.append(temp)
            ticks.append(t)
            queueLock.release()            
        else:
            queueLock.release()


threadList = ["Thread-1", "Thread-2", "Thread-3","Thread-4", "Thread-5", "Thread-6","Thread-7", "Thread-8", "Thread-9","Thread-10","Thread-11", "Thread-12", "Thread-13","Thread-14", "Thread-15", "Thread-16","Thread-17", "Thread-18", "Thread-19","Thread-20","Thread-21", "Thread-22", "Thread-23","Thread-24", "Thread-25", "Thread-26","Thread-27", "Thread-28", "Thread-29","Thread-30","Thread-31", "Thread-32", "Thread-33","Thread-34", "Thread-35", "Thread-36","Thread-37", "Thread-38", "Thread-39","Thread-40"]
#nameList = ["One", "Two", "Three", "Four", "Five"]
queueLock = threading.Lock()
workQueue = Queue.Queue(510)
threads = []
threadID = 1

# Create new threads
for tName in threadList:
    thread = myThread(threadID, tName, workQueue)
    thread.start()
    threads.append(thread)
    threadID += 1

# Fill the queue
queueLock.acquire()
for ticker in ts:
    workQueue.put(ticker)
queueLock.release()

# Wait for queue to empty
while not workQueue.empty():
    pass

# Notify threads it's time to exit
exitFlag = 1

# Wait for all threads to complete
for t in threads:
    t.join()
print "Exiting Main Thread"

print 'Calculating RSI'

# calculates RSI
for stock in data:
    index = 0
    rsi = []
    poschange = []
    negchange = []
    avggain = []
    avgloss = []
    close = stock['Adj Close'].tolist()
    for day in close:
        if index == 0:
            poschange.append('-')
            negchange.append('-')
        elif (close[index] - close[index - 1]) > 0.0:
            poschange.append(close[index] - close[index - 1])
            negchange.append(0.0)
        else:
            poschange.append(0.0)
            negchange.append(close[index - 1] - close[index])
        if index == 0:
            avggain.append('-')
            avgloss.append('-')
        elif index == 1:
            avggain.append(poschange[index])
            avgloss.append(negchange[index])
        elif index <= 14:
            avggain.append(((avggain[index - 1]*(index - 1) + poschange[index])/index))
            avgloss.append(((avgloss[index - 1]*(index - 1) + negchange[index])/index))
        else:
            avggain.append(((avggain[index - 1]*13) + poschange[index])/14)
            avgloss.append(((avgloss[index - 1]*13) + negchange[index])/14)
        if index < 14:
            rsi.append('-')
        elif avgloss[index] == 0:
            rsi.append('-')
        else:
            rsi.append(100 - (100/(1+((avggain[index])/(avgloss[index])))))     
        index += 1
    stock['RSI'] = rsi

print "Calculating ADX"

#Calculates ADX and Scenarios
for stock in data:
    high = stock.High.tolist()
    low = stock.Low.tolist()
    close = stock.Close.tolist()
    rsi = stock.RSI.tolist()
    index = 0
    TR14 = []
    PDM14 = []
    MDM14 = []
    PDI14 = []
    MDI14 = []
    DX= []
    ADX = []
    Scenario = []
    for i in range(14): 
        TR14.append(0), PDM14.append(0),MDM14.append(0),PDI14.append(0),
        MDI14.append(0), DX.append(0), ADX.append("-")
    for i in range(13): ADX.append("-")
    TR=[0]
    #+DM and -DM 
    PDM =[0]
    MDM =[0]
    for day in high:
        if index>0:
            TR.append(max([high[index]-low[index],abs(high[index]-close[index-1]),abs(low[index]-close[index-1])]))
            TR[index] = round(TR[index]*100)/100
            if high[index]-high[index-1]>low[index-1]-low[index] and high[index]-high[index-1]>0:
                PDM.append(high[index]-high[index-1])
            else: PDM.append(0)
            PDM[index] = round(PDM[index]*100)/100
            if high[index]-high[index-1]<low[index-1]-low[index] and low[index-1]-low[index]>0:
                MDM.append(low[index-1]-low[index])
            else: MDM.append(0)
            MDM[index] = round(MDM[index]*100)/100
        if index==14: 
            TR14.append(sum(TR)),MDM14.append(sum(MDM)),PDM14.append(sum(PDM))
            PDI14.append(PDM14[index]/TR14[index]*100)
            MDI14.append(MDM14[index]/TR14[index]*100)
            DX.append(abs(PDI14[index]-MDI14[index])/(PDI14[index]+MDI14[index])*100)
        if index >14: 
            TR14.append(TR14[index-1]-TR14[index-1]/14+TR[index])
            MDM14.append(MDM14[index-1]-MDM14[index-1]/14+MDM[index])
            PDM14.append(PDM14[index-1]-PDM14[index-1]/14+PDM[index])
            PDI14.append(PDM14[index]/TR14[index]*100)
            MDI14.append(MDM14[index]/TR14[index]*100)
            DX.append(abs(PDI14[index]-MDI14[index])/(PDI14[index]+MDI14[index])*100)
        if index==27: ADX.append(sum(DX)/14)
        if index>27:  ADX.append((13*ADX[index-1]+DX[index])/14)
        TR14[index] = round(TR14[index]*100)/100
        MDM14[index] = round(MDM14[index]*100)/100
        PDM14[index] = round(PDM14[index]*100)/100
        MDI14[index] = round(MDI14[index]*100)/100
        PDI14[index] = round(PDI14[index]*100)/100
        DX[index] = round(DX[index]*100)/100
        if ADX[index] <20:
            if rsi[index] > 70:
                Scenario.append(2)
            elif rsi[index] < 30:
                Scenario.append(8)
            else:
                Scenario.append(5)
        elif ADX[index]>25 and ADX[index]!='-' and PDI14[index]>MDI14[index]:
            if rsi[index] > 70:
                Scenario.append(1)
            elif rsi[index] < 30:
                Scenario.append(7)
            else:
                Scenario.append(4)
        elif ADX[index]>25 and ADX[index]!='-' and PDI14[index]<MDI14[index]:
            if rsi[index] > 70:
                Scenario.append(3)
            elif rsi[index] < 30:
                Scenario.append(9)
            else:
                Scenario.append(6)
        else: Scenario.append(0)
        try:ADX[index] = round(ADX[index]*100)/100
        except: 
            index +=1 
            continue
        index +=1
        
    if len(high)<27:
            ADX =[]
            for i in range(len(high)):
                ADX.append("-")    
    stock['ADX']= ADX
    stock['Scenario']=Scenario


#%%------------------------------Stategy 8----------------------
#Signal Iterations

def find_success_bullish(price,gap_low,gap_high):       
    open_price = price.Open.tolist()
    close_price = price.Close.tolist()
    scenario = price.Scenario.tolist()
    end = len(open_price)
    scenario_signal=0
    success_signal=0
    signal_return=[]
    scenarios=[3,6]
    tuple_list = list(product(scenarios))
    max_success = 0
    max_success_param=tuple_list[0]
    ave_signal_ret = 0
    num_signal = 0
    for param in tuple_list:
        scenarios = param[0]
        for t in range (1, end):
            scen = scenario[t-1]
            gap = open_price[t]/close_price[t-1] - 1
    #bullish,short      3 and 6
            if gap > gap_low and gap <= gap_high and scen == scenarios:
                scenario_signal +=1
                short_price = open_price[t]
                cover_price=close_price[t]
                signal_return.append(1-(cover_price/short_price))
                if cover_price/short_price < 1:
                    success_signal +=1
        if scenario_signal == 0:
            success_rate = 0
            signal_ret=0
        else:
            success_rate = float(success_signal)/float(scenario_signal)
            signal_ret=np.mean(signal_return)
        if success_rate>max_success:
            max_success = success_rate
            max_success_param=param
            ave_signal_ret = signal_ret
            num_signal = scenario_signal
            
    return (max_success,ave_signal_ret,num_signal,max_success_param)

def find_success_bearish(price,gap_low,gap_high):       
    open_price = price.Open.tolist()
    close_price = price.Close.tolist()
    scenario = price.Scenario.tolist()
    end = len(open_price)
    scenario_signal=0
    success_signal=0
    signal_return=[]
    scenarios=[4,7]
    tuple_list = list(product(scenarios))
    max_success = 0
    max_success_param=tuple_list[0]
    ave_signal_ret = 0
    num_signal=0
    for param in tuple_list:
        scenarios = param[0]
        for t in range (1, end):
            scen = scenario[t-1]
            gap = open_price[t]/close_price[t-1] - 1
    #bearish,long    4 and 7 
            if gap < -gap_low and gap >= -gap_high and scen == scenarios:
                scenario_signal +=1
                buy_price = open_price[t]
                sell_price=close_price[t]
                signal_return.append(sell_price/buy_price - 1)
                if sell_price/buy_price > 1:
                    success_signal +=1
        if scenario_signal == 0:
            success_rate = 0
            signal_ret=0
        else:
            success_rate = float(success_signal)/float(scenario_signal)
            signal_ret=np.mean(signal_return)
        if success_rate>max_success:
            max_success = success_rate
            max_success_param=param
            ave_signal_ret = signal_ret
            num_signal = scenario_signal
            
    return (max_success,ave_signal_ret,num_signal,max_success_param)
     

#%% qualified stocks   
 
def qs_list(year,data,ticks,gap_low,gap_high):
    x=np.zeros(1008,dtype={'names':['ticker','success','ave_signal_ret','scenario_signal','scenario','bullish'],'formats':['S10','float16','float16','i2','i2','b1']})
    for i in range(504):
        #print 'calculating success rate for %s' %ticks[i]
        v=find_success_bullish(data[i].loc[str(year-5)+'0101':str(year-1)+'1231'],gap_low,gap_high)
        x[i]=(ticks[i],v[0],v[1],v[2],v[3][0],True)
        v=find_success_bearish(data[i].loc[str(year-5)+'0101':str(year-1)+'1231'],gap_low,gap_high)
        x[i+504]=(ticks[i],v[0],v[1],v[2],v[3][0],False)
    performance=np.sort(x,order='success')[::-1]
    #historical stat including('ticker','success','ave_signal_ret','scenario_signal','bullish')
    qualified_stocks=[]
    for i in range(len(performance)):
        if performance[i]['success']>=.5 and performance[i]['ave_signal_ret']>0 and performance[i]['scenario_signal']>=20:
            qualified_stocks.append(performance[i])
            #qualified stocks stat, including('ticker','success','ave_signal_ret','scenario_signal','bullish')
    print 'number of qualified stocks in '+str(year)+': %i' %len(qualified_stocks)
    return qualified_stocks

#%%portfolio performance

def portfolio(year,all_data,qualified_stocks,gap_low,gap_high):
    t_index=[]
    scenarios=[]
    for i in range(len(qualified_stocks)):
        temp=ticks.index(qualified_stocks[i][0])
        t_index.append(temp)
        scenarios.append(qualified_stocks[i]['scenario'])
    data=[]
    for i in range(len(all_data)):
        data.append(all_data[i].loc[year+'0101':year+'1231'])   
    asset=1
    scenario_signal=0
    success_signal=0
    for t in range(1,len(data[ticks.index('YHOO')])):
        trade=False
        i=0
        while (not trade) and i<len(t_index):
            scen =  data[t_index[i]].Scenario[t-1]
            gap = data[t_index[i]].Open[t]/data[t_index[i]].Close[t-1] - 1
            if qualified_stocks[i]['bullish']:
                if gap > gap_low and gap < gap_high and scen == scenarios[i]:
                    scenario_signal +=1
                    short_price = data[t_index[i]].Open[t]
                    cover_price=data[t_index[i]].Close[t]
                    asset = (2-(cover_price/short_price))*asset
                    if 1 > cover_price/short_price:
                        success_signal +=1
                    trade=True
            else:
                if gap < -gap_low and gap > -gap_high and scen == scenarios[i]:
                    scenario_signal +=1
                    buy_price = data[t_index[i]].Open[t]
                    sell_price = data[t_index[i]].Close[t]
                    if sell_price/buy_price > 1:
                        success_signal +=1
                    asset=(sell_price / buy_price)*asset
                    trade=True
            i+=1
    if scenario_signal == 0:
        success_rate = 0
    else:
        success_rate = float(success_signal)/float(scenario_signal)
    return [asset-1, success_rate]
#%%finding qualfied stocks  

def main(year,data,ticks,gap_low,gap_high):
    s='for gap '+repr(int(gap_low*100))+'% to '+repr(int(gap_high*100))+'%'
    print s
    
    qualified_stocks=qs_list(year,data,ticks,gap_low,gap_high)
    
    l=portfolio(str(year),data,qualified_stocks,gap_low,gap_high)
    print 'Return in '+str(year)+' is %f'%l[0]
    print 'Success Rate in '+str(year)+' is %f'%l[1]
    
    return qualified_stocks


#%%insert corresponding year to get portfolio data. uncomment the for loops to see 
#%%qualified stocks. True is bullish and false is bearish for the last parameter
gap_low=0.01    
gap_high=0.02
qualified_stocks=main(2016,data,ticks,gap_low,gap_high)
for item in qualified_stocks: print item
gap_low=0.02
gap_high=0.03  
qualified_stocks=main(2016,data,ticks,gap_low,gap_high)
for item in qualified_stocks: print item
gap_low=0.03
gap_high=0.04
qualified_stocks=main(2016,data,ticks,gap_low,gap_high)
for item in qualified_stocks: print item
for year in range(2011,2016):
    gap_low=0.01    
    gap_high=0.02
    qualified_stocks=main(year,data,ticks,gap_low,gap_high)
    gap_low=0.02    
    gap_high=0.03
    qualified_stocks=main(year,data,ticks,gap_low,gap_high)
    gap_low=0.03    
    gap_high=0.04
    qualified_stocks=main(year,data,ticks,gap_low,gap_high)

Hello guys

We want to improve the productivity of your work so we would like to ask you to read some information about bitbucket and git.

Git (/ɡɪt/) is a version control system that is widely used for software development and other version control tasks. 
It is a distributed revision control system with an emphasis on speed, data integrity, and support for distributed, non-linear workflows.
(For more info look         https://en.wikipedia.org/wiki/Git_(software)         )

To get a free and easy tutorial, please follow this link:
https://www.atlassian.com/git/?utm_source=bitbucket&utm_medium=link&utm_campaign=help_dropdown&utm_content=learn_git

For more tutorials visit:
https://confluence.atlassian.com/bitbucket/bitbucket-tutorials-teams-in-space-training-ground-755338051.html


Let’s do a quick exercise in order for you guys to familiarize yourself with the bitbucket:
1. Create a file *your_name*.txt in the following folder BRA_summer_internship_2016/internship/
2. Write a sentence about anything
3. Open the file of another contestant and write a short comment about his or her sentence
4. All adjustments have to be done through bitbucket only
Feel free to ask us any questions

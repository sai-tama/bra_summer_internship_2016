# -*- coding: utf-8 -*-
"""
Created on Thu Jun 23 17:24:46 2016

@author: renguanqi
"""
#%%
import csv
import threading
import Queue
import numpy as np
import math
import scipy
from pandas.io.data import DataReader
from itertools import product
from scipy import stats

#%% 
#downlaod data

with open('constituents.csv', 'rb') as f:
    reader = csv.reader(f)
    tickers=list(reader)
ticks=[]
ts=[]
for i in range(0,504):
    ts.extend(tickers[i])

start="2006/1/1"

exitFlag = 0

data=[]

class myThread (threading.Thread):
    def __init__(self, threadID, name, q):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.q = q
    def run(self):
        print "Starting " + self.name
        process_data(self.name, self.q, ticks)
        queueLock.acquire()        
        print "Exiting " + self.name
        queueLock.release()

def process_data(threadName, q,ticks):
    while not exitFlag:
        queueLock.acquire()
        if not workQueue.empty():
            t = q.get()
            print threadName+" downloading data of %s" % t
            queueLock.release()
            temp=DataReader(t,"yahoo",start)
            queueLock.acquire()
            data.append(temp)
            ticks.append(t)
            queueLock.release()            
        else:
            queueLock.release()


threadList = ["Thread-1", "Thread-2", "Thread-3","Thread-4", "Thread-5", "Thread-6","Thread-7", "Thread-8", "Thread-9","Thread-10","Thread-11", "Thread-12", "Thread-13","Thread-14", "Thread-15", "Thread-16","Thread-17", "Thread-18", "Thread-19","Thread-20","Thread-21", "Thread-22", "Thread-23","Thread-24", "Thread-25", "Thread-26","Thread-27", "Thread-28", "Thread-29","Thread-30","Thread-31", "Thread-32", "Thread-33","Thread-34", "Thread-35", "Thread-36","Thread-37", "Thread-38", "Thread-39","Thread-40"]
#nameList = ["One", "Two", "Three", "Four", "Five"]
queueLock = threading.Lock()
workQueue = Queue.Queue(510)
threads = []
threadID = 1

# Create new threads
for tName in threadList:
    thread = myThread(threadID, tName, workQueue)
    thread.start()
    threads.append(thread)
    threadID += 1

# Fill the queue
queueLock.acquire()
for ticker in ts:
    workQueue.put(ticker)
queueLock.release()

# Wait for queue to empty
while not workQueue.empty():
    pass

# Notify threads it's time to exit
exitFlag = 1

# Wait for all threads to complete
for t in threads:
    t.join()
print "Exiting Main Thread"


#%%------------------------------Stategy 6----------------------
#Signal Iterations

def find_success_bullish(price):       
    open_price = price.Open.tolist()
    close_price = price.Close.tolist()
    end = len(open_price)
    Angle = [20,25,30,35,40]
    period = [5,6,7,8,9,10]
    tuple_list = list(product(Angle,period))
    max_success=0
    max_success_param=tuple_list[0]
    ave_signal_ret=0
    num_signal=0
    for param in tuple_list:
        angle_signal=0
        success_signal=0
        dailyreturn = []
        Angle = param[0]
        period = param[1]
    
        for t in range (1+period, end):
            linregressopen = []
            linregressclose = []
            for j in range (t - period -1, t-1):
                linregressopen.append(open_price[j])
                linregressclose.append(close_price[j])
            slope, intercept, r_value, p_value, slope_std_error = stats.linregress(linregressopen, linregressclose)
            angle = np.arctan(slope)

#bullish,short      
            if  angle > Angle:
                angle_signal+=1
                short_price = open_price[t]
                cover_price=close_price[t]
                dailyreturn.append(1-cover_price/short_price)
                if (1-(cover_price/short_price)) > 0:
                    success_signal +=1
                else:
                    pass
            else:
                pass
        if angle_signal == 0:
            success_rate = 0
        else:
            success_rate = success_signal/angle_signal
        if success_rate>max_success:
                max_success=success_rate
                max_success_param=param
                sum_of_ret=0
                for i in range(len(dailyreturn)):
                    sum_of_ret+=dailyreturn[i]
                ave_signal_ret=sum_of_ret/float(angle_signal)
                num_signal=angle_signal

    return (max_success,ave_signal_ret,max_success_param,num_signal)
    
#%%
def find_success_bearish(price):       
    open_price = price.Open.tolist()
    close_price = price.Close.tolist()
    end = len(open_price)
    Angle = [20,25,30,35,40]
    period = [5,6,7,8,9,10]
    tuple_list = list(product(Angle,period))
    max_success=0
    max_success_param=tuple_list[0]
    ave_signal_ret=0
    num_signal=0;
    for param in tuple_list:
        angle_signal=0
        success_signal=0
        dailyreturn = []
        Angle = param[0]
        period = param[1]
        
        for t in range (1+period, end):
            linregressopen = []
            linregressclose = []
            for j in range (t - period -1, t-1):
                linregressopen.append(open_price[j])
                linregressclose.append(close_price[j])
            slope, intercept, r_value, p_value, slope_std_error = stats.linregress(linregressopen, linregressclose)
            angle = np.arctan(slope)

#bearish, long    
            if angle < -1*Angle:
                angle_signal+=1
                buy_price = open_price[t]
                sell_price=close_price[t]
                dailyreturn.append(sell_price / buy_price-1)
                if sell_price/buy_price - 1 > 0:
                    success_signal += 1
                else:
                    pass
            else:
                pass
        if angle_signal == 0:
            success_rate = 0
        else:
            success_rate = success_signal/angle_signal
        if success_rate>max_success:
                max_success=success_rate
                max_success_param=param
                sum_of_ret=0
                for i in range(len(dailyreturn)):
                    sum_of_ret+=dailyreturn[i]
                ave_signal_ret=sum_of_ret/float(angle_signal)
                num_signal=angle_signal

    return (max_success,ave_signal_ret,max_success_param,num_signal)


#%% qualified stocks   
 
def qs_list(year,data,ticks):
    x=np.zeros(1008,dtype={'names':['ticker','success','ave_signal_ret','Angle','period','angle_signal','bullish'],'formats':['S10','float16','float16','float16','float16','i2','b1']})
    for i in range(504):
        print 'calculating success rate for %s' %ticks[i]
        v=find_success_bullish(data[i].loc[str(year-5)+'0101':str(year-1)+'1231'])
        x[i]=(ticks[i],v[0],v[1],v[2][0],v[2][1],v[3],True)
        v=find_success_bearish(data[i].loc[str(year-5)+'0101':str(year-1)+'1231'])
        x[i+504]=(ticks[i],v[0],v[1],v[2][0],v[2][1],v[3],False)
    performance=np.sort(x,order='success')[::-1]#historical stat including('ticker','success','ave_signal_ret','entry','stop_loss','price_target','gap_signal','bullish')
    qualified_stocks=[]
    for i in range(len(performance)):
        if performance[i]['success']>=.75 and performance[i]['ave_signal_ret']>=0.0 and performance[i]['angle_signal']>4:
            qualified_stocks.append(performance[i])#qualified stocks stat, including('ticker','sharpe','ave_signal_ret','entry','stop_loss','price_target','gap_signal','bullish')
    print 'number of qualified stocks in '+str(year)+': %i' %len(qualified_stocks)
    return qualified_stocks

#%%portfolio performance

def portfolio(year,all_data,qualified_stocks):
    t_index=[]
    Angle=[]
    period=[]
    for i in range(len(qualified_stocks)):
        temp=ticks.index(qualified_stocks[i][0])
        t_index.append(temp)
        Angle.append(qualified_stocks[i]['Angle'])
        period.append(qualified_stocks[i]['period'])
    data=[]
    for i in range(len(all_data)):
        data.append(all_data[i].loc[year+'0101':year+'1231'])   
    asset=[1]
    angle_signal=0
    success_signal=0
    for t in range(11,len(data[ticks.index('YHOO')])):
        trade=False
        i=0
        while (not trade) and i<len(t_index):
            linregressopen = []
            linregressclose = []
            for k in range (t - 1 - period[i], t-1):
                linregressopen.append(data[t_index[i]].Open[k])
                linregressclose.append(data[t_index[i]].Close[k])
            slope, intercept, r_value, p_value, slope_std_error = stats.linregress(linregressopen, linregressclose)
            angle = np.arctan(slope)
            if qualified_stocks[i]['bullish']:
                if angle > Angle[i]:
                    angle_signal += 1
                    short_price = data[t_index[i]].Open[t]
                    cover_price=data[t_index[i]].Close[t]
                    asset.append((2-cover_price/short_price)*asset[t-1])
                    if 1-(cover_price/short_price) > 0:
                        success_signal += 1
                    trade=True
            else:
                if angle < -1*Angle[i]:
                    buy_price = data[t_index[i]].Open[t]
                    sell_price=data[t_index[i]].Close[t]
                    if sell_price/buy_price - 1 > 0:
                        success_signal += 1
                    asset.append(sell_price / buy_price*asset[t-1])
                    trade=True
            i+=1
        if not trade:
            asset.append(asset[t-1])

    if angle_signal == 0:
        success_rate = 0
    else:
        success_rate = success_signal/angle_signal
    return [asset[len(asset)-1]-1, success_rate]
#%%finding qualfied stocks  

def main(year,data,ticks):

    qualified_stocks=qs_list(year,data,ticks)
    
    l=portfolio(str(year),data,qualified_stocks)
    print 'Return in '+str(year)+' is %f'%l[0]
    print 'Success Rate in '+str(year)+' is %f'%l[1]
    
    return qualified_stocks

#%%insert corresponding year to get portfolio data. uncomment the for loops to see 
#%%qualified stocks. True is bullish and false is bearish for the last parameter

qualified_stocks=main(2016,data,ticks)
for item in qualified_stocks: print item
qualified_stocks=main(2015,data,ticks)
qualified_stocks=main(2014,data,ticks)
qualified_stocks=main(2013,data,ticks)
qualified_stocks=main(2012,data,ticks)
qualified_stocks=main(2011,data,ticks)

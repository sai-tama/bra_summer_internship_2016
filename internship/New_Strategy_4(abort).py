# -*- coding: utf-8 -*-
"""
Created on Thu Jun 23 17:24:46 2016

@author: renguanqi
"""
#%%
import csv
import threading
import Queue
import numpy as np
import math
#import matplotlib.pyplot as plt

from pandas.io.data import DataReader
from itertools import product

#%% 
#downlaod data

with open('constituents.csv', 'rb') as f:
    reader = csv.reader(f)
    tickers=list(reader)
ticks=[]
ts=[]
for i in range(0,504):
    ts.extend(tickers[i])

start="2006/1/1"

exitFlag = 0

data=[]

class myThread (threading.Thread):
    def __init__(self, threadID, name, q):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.q = q
    def run(self):
        print "Starting " + self.name
        process_data(self.name, self.q, ticks)
        queueLock.acquire()        
        print "Exiting " + self.name
        queueLock.release()

def process_data(threadName, q,ticks):
    while not exitFlag:
        queueLock.acquire()
        if not workQueue.empty():
            t = q.get()
            print threadName+" downloading data of %s" % t
            queueLock.release()
            temp=DataReader(t,"yahoo",start)
            queueLock.acquire()
            data.append(temp)
            ticks.append(t)
            queueLock.release()            
        else:
            queueLock.release()


threadList = ["Thread-1", "Thread-2", "Thread-3","Thread-4", "Thread-5", "Thread-6","Thread-7", "Thread-8", "Thread-9","Thread-10","Thread-11", "Thread-12", "Thread-13","Thread-14", "Thread-15", "Thread-16","Thread-17", "Thread-18", "Thread-19","Thread-20","Thread-21", "Thread-22", "Thread-23","Thread-24", "Thread-25", "Thread-26","Thread-27", "Thread-28", "Thread-29","Thread-30","Thread-31", "Thread-32", "Thread-33","Thread-34", "Thread-35", "Thread-36","Thread-37", "Thread-38", "Thread-39","Thread-40"]
#nameList = ["One", "Two", "Three", "Four", "Five"]
queueLock = threading.Lock()
workQueue = Queue.Queue(510)
threads = []
threadID = 1

# Create new threads
for tName in threadList:
    thread = myThread(threadID, tName, workQueue)
    thread.start()
    threads.append(thread)
    threadID += 1

# Fill the queue
queueLock.acquire()
for ticker in ts:
    workQueue.put(ticker)
queueLock.release()

# Wait for queue to empty
while not workQueue.empty():
    pass

# Notify threads it's time to exit
exitFlag = 1

# Wait for all threads to complete
for t in threads:
    t.join()
print "Exiting Main Thread"

#%%performance analysis function
def mean(ret):
    r=np.asarray(ret)
    return r.mean()*252
    
def success(ret):
    count = 0
    for item in ret:
        if item >0: count+=1
    if len(ret) == 0: return 0
    else: return float(count)/float(len(ret))
#%%------------------------------Stategy 7----------------------
#Signal Iterations

def find_success_bullish(price,gap_low,gap_high):       
    open_price = price.Open.tolist()
    close_price = price.Close.tolist()
    #high_price = price.High.tolist()
    #low_price= price.Low.tolist()
    end = len(open_price)
    vol_percent = [0.005,.01,.015,.02]
    vol_period = [5,10,20,30,50]
    tuple_list = list(product(vol_percent,vol_period))
    max_success=0
    max_success_param=tuple_list[0]
    ave_signal_ret=0
    num_signal=0
    rollingvol= np.std(close_price)
    for param in tuple_list:
        vol_signal=0
        vollist=[]
        vol_return = []
        vol_percent = param[0]
        vol_period = param[1]

    
        for t in range (1, end):
            ocgap = open_price[t]/close_price[t-1] - 1
            try:
                for item in range(1,vol_period+1): vollist.append(close_price[t-item])
                volatility = np.std(vollist)
                vollist=[]
            except: continue
#bullish,short      
            if ocgap > gap_low and ocgap<=gap_high and volatility>rollingvol*(1+vol_percent):
                vol_signal+=1
                short_price = open_price[t]
                cover_price=close_price[t]
                vol_return.append(1-cover_price/short_price)
                   
            
        if success(vol_return)>max_success:
                max_success=success(vol_return)
                max_success_param=param
                sum_of_ret=0
                for i in range(len(vol_return)):
                    sum_of_ret+=vol_return[i]
                ave_signal_ret=sum_of_ret/float(len(vol_return))
                num_signal=len(vol_return)

    #print  max_success,ave_signal_ret,max_success_param,num_signal
    return (max_success,ave_signal_ret,max_success_param,num_signal)
    
#%%
def find_success_bearish(price,gap_low,gap_high):       
    open_price = price.Open.tolist()
    close_price = price.Close.tolist()
    #high_price = price.High.tolist()
    #low_price= price.Low.tolist()
    end = len(open_price)
    vol_percent = [0.005,.01,.015,.02]
    vol_period = [5,10,20,30,50]
    tuple_list = list(product(vol_percent,vol_period))
    max_success=0
    max_success_param=tuple_list[0]
    ave_signal_ret=0
    num_signal=0
    rollingvol= np.std(close_price)
    for param in tuple_list:
        #vol_signal=0
        vollist=[]
        vol_return = []
        vol_percent = param[0]
        vol_period = param[1]
        
        for t in range (1, end):
            ocgap = open_price[t]/close_price[t-1] - 1
            try:
                for item in range(1,vol_period+1): vollist.append(close_price[t-item])
                volatility = np.std(vollist)
                vollist=[]
            except: continue
#bearish, long    
            if ocgap<-gap_low and ocgap>=-gap_high and volatility>rollingvol*(1+vol_percent):
                buy_price = open_price[t]
                sell_price=close_price[t]
                vol_return.append(sell_price / buy_price-1)
            

        if success(vol_return)>max_success:
                max_success=success(vol_return)
                max_success_param=param
                sum_of_ret=0
                for i in range(len(vol_return)):
                    sum_of_ret+=vol_return[i]
                ave_signal_ret=sum_of_ret/float(len(vol_return))
                num_signal=len(vol_return)

    #print  max_success,ave_signal_ret,max_success_param,num_signal
    return (max_success,ave_signal_ret,max_success_param,num_signal)


#%% qualified stocks   
 
def qs_list(year,data,ticks,gap_low,gap_high):
    x=np.zeros(1008,dtype={'names':['ticker','success','ave_signal_ret','vol_percent','vol_period','vol_signal','bullish'],'formats':['S10','float16','float16','float16','float16','i2','b1']})
    for i in range(504):
        #print 'calculating sharpe for %s' %ticks[i]
        v=find_success_bullish(data[i].loc[str(year-5)+'0101':str(year-1)+'1231'],gap_low,gap_high)
        x[i]=(ticks[i],v[0],v[1],v[2][0],v[2][1],v[3],True)
        v=find_success_bearish(data[i].loc[str(year-5)+'0101':str(year-1)+'1231'],gap_low,gap_high)
        x[i+504]=(ticks[i],v[0],v[1],v[2][0],v[2][1],v[3],False)
    performance=np.sort(x,order='success')[::-1]#historical stat including('ticker','sharpe','ave_signal_ret','entry','stop_loss','price_target','gap_signal','bullish')
    qualified_stocks=[]
    for i in range(len(performance)):
        if performance[i]['success']>=.6 and performance[i]['ave_signal_ret']>=0.0 and performance[i]['vol_signal']>=5:
            qualified_stocks.append(performance[i])#qualified stocks stat, including('ticker','sharpe','ave_signal_ret','entry','stop_loss','price_target','gap_signal','bullish')
    print 'number of qualified stocks in '+str(year)+': %i' %len(qualified_stocks)
    return qualified_stocks

#%%portfolio performance

def portfolio(year,all_data,qualified_stocks,gap_low,gap_high):
    t_index=[]
    #entry=[]
    #stop_loss=[]
    #price_target=[]
    vol_percent=[]
    vol_period=[]
    vollist=[]
    rollingvol = []
    for i in range(len(qualified_stocks)):
        temp=ticks.index(qualified_stocks[i][0])
        t_index.append(temp)
        vol_percent.append(qualified_stocks[i]['vol_percent'])
        vol_period.append(qualified_stocks[i]['vol_period'])
    data=[]
    for i in range(len(all_data)):
        data.append(all_data[i].loc[str(int(year)-5)+'0101':year+'1231']) 
    asset=[1]
    for i in range(len(qualified_stocks)):
        for t in range(1,len(data[ticks.index('YHOO')])): vollist.append(data[t_index[i]].Close[t])
        rollingvol.append(np.std(vollist))
        vollist=[]
            
    ret=[]
    #this_year_start_index=len(data[ticks.index('YHOO')].loc[str(int(year)-5)+'0101':str(int(year)-1)+'1231'])
    #this_year_end_index=len(data[ticks.index('YHOO')])  
    vollist2=[]
    volatility = []  
    for t in range(1,len(data[ticks.index('YHOO')])):
        trade=False
        i=0
        while (not trade) and i<len(t_index):
            ocgap=data[t_index[i]].Open[t]/data[t_index[i]].Close[t-1]-1
            try:
                for item in range(1,vol_period[i]+1): vollist2.append(data[t_index[i]].Close[t-item])
                volatility=np.std(vollist2)
                vollist2=[]
            except: continue
            if qualified_stocks[i]['bullish']:
                if ocgap > gap_low and ocgap<=gap_high and volatility>rollingvol[i]*(1+vol_percent[i]):
                    short_price = data[t_index[i]].Open[t]
                    cover_price=data[t_index[i]].Close[t]
                    asset.append((2-cover_price/short_price)*asset[t-1])
                    ret.append(1-cover_price/short_price)
                    trade=True
            else:
                if ocgap<-gap_low and ocgap>=-gap_high and volatility>rollingvol[i]*(1+vol_percent[i]):
                    buy_price = data[t_index[i]].Open[t]
                    sell_price=data[t_index[i]].Close[t]
                    ret.append(sell_price/buy_price-1)
                    asset.append(sell_price / buy_price*asset[t-1])
                    trade=True
            i+=1
        
        if not trade:
            asset.append(asset[t-1])
            ret.append(0)
    #plt.plot(asset)
    return [asset[len(asset)-1]-1,success(ret)]
#%%finding qualfied stocks  

def main(year,data,ticks,gap_low,gap_high):
    s='for gap '+repr(int(gap_low*100))+'% to '+repr(int(gap_high*100))+'%'
    print s
    
    qualified_stocks=qs_list(year,data,ticks,gap_low,gap_high)
    
    l=portfolio(str(year),data,qualified_stocks,gap_low,gap_high)
    print 'Return in '+str(year)+' is %f'%l[0]
    print 'Success in '+str(year)+' is %f'%l[1]
    
    return qualified_stocks

#%%insert corresponding year to get portfolio data. uncomment the for loops to see 
#%%qualified stocks. True is bullish and false is bearish for the last parameter
gap_low=0.01    
gap_high=0.02
qualified_stocks=main(2016,data,ticks,gap_low,gap_high)
for item in qualified_stocks: print item
gap_low=0.02
gap_high=0.03  
qualified_stocks=main(2016,data,ticks,gap_low,gap_high)
for item in qualified_stocks: print item
gap_low=0.03
gap_high=0.04
qualified_stocks=main(2016,data,ticks,gap_low,gap_high)
for item in qualified_stocks: print item
for year in range(2011,2016):
    gap_low=0.01    
    gap_high=0.02
    qualified_stocks=main(year,data,ticks,gap_low,gap_high)
    gap_low=0.02    
    gap_high=0.03
    qualified_stocks=main(year,data,ticks,gap_low,gap_high)
    gap_low=0.03    
    gap_high=0.04
    qualified_stocks=main(year,data,ticks,gap_low,gap_high)


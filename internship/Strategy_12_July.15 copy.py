# -*- coding: utf-8 -*-
"""
Created on Thu Jun 30 14:22:12 2016

@author: renguanqi
"""

#%%
import csv
import threading
import Queue
import numpy as np
import math
#import matplotlib.pyplot as plt

from pandas.io.data import DataReader
from itertools import product

#%% 
#download data

with open('constituents.csv', 'rb') as f:
    reader = csv.reader(f)
    tickers=list(reader)
ticks=[]
ts=[]
for i in range(0,504):
    ts.extend(tickers[i])

start="2006/1/1"

exitFlag = 0

data=[]

class myThread (threading.Thread):
    def __init__(self, threadID, name, q):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.q = q
    def run(self):
        print "Starting " + self.name
        process_data(self.name, self.q, ticks)
        queueLock.acquire()        
        print "Exiting " + self.name
        queueLock.release()

def process_data(threadName, q,ticks):
    while not exitFlag:
        queueLock.acquire()
        if not workQueue.empty():
            t = q.get()
            print threadName+" downloading data of %s" % t
            queueLock.release()
            temp=DataReader(t,"yahoo",start)
            queueLock.acquire()
            data.append(temp)
            ticks.append(t)
            queueLock.release()            
        else:
            queueLock.release()


threadList = ["Thread-1", "Thread-2", "Thread-3","Thread-4", "Thread-5", "Thread-6","Thread-7", "Thread-8", "Thread-9","Thread-10","Thread-11", "Thread-12", "Thread-13","Thread-14", "Thread-15", "Thread-16","Thread-17", "Thread-18", "Thread-19","Thread-20","Thread-21", "Thread-22", "Thread-23","Thread-24", "Thread-25", "Thread-26","Thread-27", "Thread-28", "Thread-29","Thread-30","Thread-31", "Thread-32", "Thread-33","Thread-34", "Thread-35", "Thread-36","Thread-37", "Thread-38", "Thread-39","Thread-40"]
#nameList = ["One", "Two", "Three", "Four", "Five"]
queueLock = threading.Lock()
workQueue = Queue.Queue(510)
threads = []
threadID = 1

# Create new threads
for tName in threadList:
    thread = myThread(threadID, tName, workQueue)
    thread.start()
    threads.append(thread)
    threadID += 1

# Fill the queue
queueLock.acquire()
for ticker in ts:
    workQueue.put(ticker)
queueLock.release()

# Wait for queue to empty
while not workQueue.empty():
    pass

# Notify threads it's time to exit
exitFlag = 1

# Wait for all threads to complete
for t in threads:
    t.join()
print "Exiting Main Thread"

#%%performance analysis function
def mean(ret):
    r=np.asarray(ret)
    return r.mean()*252
    
def sharpe(ret):
    print "cal sharpe.."
    r=np.asarray(ret)
    return r.mean()/r.std()*math.sqrt(252)
    
#%%signal
def signal(close,open):
    signal=[(0,0.0)]
    for t in range(1,len(close)):
        ret=close[t]/open[t]-1
        if ret>=0:
            if signal[t-1][0]>=0:
                temp=(signal[t-1][0]+1,(float(signal[t-1][0])*signal[t-1][1]+ret)/float(signal[t-1][0]+1))
                signal.append(temp)
            if signal[t-1][0]<0:
                temp=(1,ret)
                signal.append(temp)
        if ret<0:
            if signal[t-1][0]<=0:
                temp=(signal[t-1][0]-1,(float(signal[t-1][0])*signal[t-1][1]+ret)/float(-signal[t-1][0]+1))
                signal.append(temp)
            if signal[t-1][0]>0:
                temp=(-1,ret)
                signal.append(temp)
    return signal
#%%------------------------------Stategy 1----------------------
#Signal Iterations

def find_sharpe_bullish(price,consecutiveness):       
    open_price = price.Open.tolist()
    close_price = price.Close.tolist()
    high_price = price.High.tolist()
    low_price= price.Low.tolist()
    end = len(open_price)
    #entry = [0.01,0.015,0.02,0.025,0.03,0.035]
    entry = [0]
    stop_loss = [0.01,0.015,0.02,0.025]
    price_target = [0.015,0.02,0.025,0.03]
    multiplier=[0.005,0.01,0.015,0.02]
    tuple_list = list(product(entry,stop_loss,price_target,multiplier))
    max_sharpe=0
    max_sharpe_param=tuple_list[0]
    ave_signal_ret=0
    num_signal=0
    signals=signal(close_price,open_price)
    for param in tuple_list:
        sig=0
        ret = []
        entry = param[0]
        stop_loss = param[1]
        price_target = param[2]
        multiplier=param[3]

    
        for t in range (1, end):      
            if signals[t][0]>consecutiveness and signals[t][1]>=multiplier:   
                if high_price[t]>open_price[t]*(1+entry):
                    sig+=1
                    short_price = open_price[t]*(1+entry)
                    exit_target = short_price*(1-price_target)
                    if high_price[t]>=short_price*(1+stop_loss):
                        cover_price=short_price*(1+stop_loss)                        
                    else:
                        if low_price[t]<exit_target:
                            cover_price=exit_target
                        else:
                            cover_price=close_price[t]
                    ret.append(1-cover_price/short_price)
            else:
                ret.append(0)
        if sharpe(ret)>max_sharpe:
                max_sharpe=sharpe(ret)
                max_sharpe_param=param
                sum_of_ret=0
                for i in range(len(ret)):
                    sum_of_ret+=ret[i]
                ave_signal_ret=sum_of_ret/float(sig)
                num_signal=sig

    return (max_sharpe,ave_signal_ret,max_sharpe_param,num_signal)
    
#%%
def find_sharpe_bearish(price,consecutiveness):       
    open_price = price.Open.tolist()
    close_price = price.Close.tolist()
    high_price = price.High.tolist()
    low_price= price.Low.tolist()
    end = len(open_price)
    #entry = [0,0.005,0.01,0.015,0.02,0.025,0.03,0.035] 
    entry = [0]
    stop_loss = [0.01,0.015,0.02,0.025]
    price_target = [0.015,0.02,0.025,0.03]
    multiplier=[0.005,0.01,0.015,0.02]
    tuple_list = list(product(entry,stop_loss,price_target,multiplier))
    max_sharpe=0
    max_sharpe_param=tuple_list[0]
    ave_signal_ret=0
    num_signal=0;
    signals=signal(close_price,open_price)
    for param in tuple_list:
        sig=0
        ret = []
        entry = param[0]
        stop_loss = param[1]
        price_target = param[2]
        multiplier=param[3]

        
        for t in range (1, end):   
            if signals[t][0]<-consecutiveness and signals[t][1]<=-multiplier:                
                if low_price[t]<open_price[t]*(1-entry):
                    sig+=1
                    buy_price = open_price[t]*(1-entry)
                    exit_target = open_price*(1+price_target)
                    if low_price[t]<=buy_price*(1-stop_loss):
                        sell_price=buy_price*(1-stop_loss)                        
                    else:
                        if high_price[t]>exit_target:
                            sell_price=exit_target
                        else:
                            sell_price=close_price[t]
                    ret.append(sell_price / buy_price-1)
            else:
                ret.append(0)

        if sharpe(ret)>max_sharpe:
                max_sharpe=sharpe(ret)
                max_sharpe_param=param
                sum_of_ret=0
                for i in range(len(ret)):
                    sum_of_ret+=ret[i]
                ave_signal_ret=sum_of_ret/float(sig)
                num_signal=sig

    return (max_sharpe,ave_signal_ret,max_sharpe_param,num_signal)


#%% qualified stocks   
 
def qs_list(year,data,ticks,consecutiveness):
    x=np.zeros(1008,dtype={'names':['ticker','sharpe','ave_signal_ret','entry','stop_loss','price_target','multiplier','num_signal','bullish'],'formats':['S10','float16','float16','float16','float16','float16','float16','i2','b1']})
    for i in range(504):
        #print 'calculating sharpe for %s' %ticks[i]
        v=find_sharpe_bullish(data[i].loc[str(year-5)+'0101':str(year-1)+'1231'],consecutiveness)
        x[i]=(ticks[i],v[0],v[1],v[2][0],v[2][1],v[2][2],v[2][3],v[3],True)
        v=find_sharpe_bearish(data[i].loc[str(year-5)+'0101':str(year-1)+'1231'],consecutiveness)
        x[i+504]=(ticks[i],v[0],v[1],v[2][0],v[2][1],v[2][2],v[2][3],v[3],False)
    performance=np.sort(x,order='sharpe')[::-1]#historical stat including('ticker','sharpe','ave_signal_ret','entry','stop_loss','price_target','gap_signal','bullish')
    qualified_stocks=[]
    for i in range(len(performance)):
        if performance[i]['sharpe']>=1.2 and performance[i]['ave_signal_ret']>=0.004 and performance[i]['num_signal']>=15:
            qualified_stocks.append(performance[i])#qualified stocks stat, including('ticker','sharpe','ave_signal_ret','entry','stop_loss','price_target','gap_signal','bullish')
    print 'number of qualified stocks in '+str(year)+': %i' %len(qualified_stocks)
    return qualified_stocks
    #return performance

#%%portfolio performance

def portfolio(year,all_data,qualified_stocks,consecutiveness):
    t_index=[]
    entry=[]
    stop_loss=[]
    price_target=[]
    multiplier=[]
    for i in range(len(qualified_stocks)):
        temp=ticks.index(qualified_stocks[i][0])
        t_index.append(temp)
        entry.append(qualified_stocks[i]['entry'])
        stop_loss.append(qualified_stocks[i]['stop_loss'])
        price_target.append(qualified_stocks[i]['price_target'])
        multiplier.append(qualified_stocks[i]['multiplier'])
    data=[]
    signals=[]
    for i in t_index:
        data.append(all_data[i].loc[year+'0101':year+'1231'])   
    for i in range(len(t_index)):
        signals.append(signal(data[i].Close,data[i].Open))
    asset=[1]
    ret=[]
    for t in range(1,len(all_data[ticks.index('YHOO')].loc[year+'0101':year+'1231'])):
        trade=False
        i=0
        while (not trade) and i<len(t_index):
            if qualified_stocks[i]['bullish']:
                if signals[i][t][0]>consecutiveness and signals[i][t][1]>=multiplier[i]:
                    if data[i].High[t]>data[i].Open[t]*(1+entry[i]):            
                        short_price = data[i].Open[t]*(1+entry[i])
                        exit_target = short_price*(1-price_target[i])
                        if data[i].High[t]>=short_price*(1+stop_loss[i]):
                            cover_price=short_price*(1+stop_loss[i])                            
                        else:
                            if data[i].Low[t]<exit_target:
                                cover_price=exit_target
                            else:
                                cover_price=data[i].Close[t]
                        asset.append((2-cover_price/short_price)*asset[t-1])
                        ret.append(1-cover_price/short_price)
                        trade=True
            else:
                if signals[i][t][0]<-consecutiveness and signals[i][t][1]<=-multiplier[i]:
                    if data[i].Low[t]<data[i].Open[t]*(1-entry[i]):
                        buy_price = data[i].Open[t]*(1-entry[i])
                        exit_target = buy_price*(1+price_target[i])
                        if data[i].Low[t]<=buy_price*(1-stop_loss[i]):
                            sell_price=buy_price*(1-stop_loss[i])                            
                        else:
                            if data[i].High[t]>exit_target:
                                sell_price=exit_target
                            else:
                                sell_price=data[i].Close[t]
                        ret.append(sell_price/buy_price-1)
                        asset.append(sell_price / buy_price*asset[t-1])
                        trade=True
            i+=1
        if not trade:
            asset.append(asset[t-1])
            ret.append(0)
    #plt.plot(asset)
    return [asset[len(asset)-1]-1,sharpe(ret)]
#%%finding qualfied stocks  

def main(year,data,ticks,consecutiveness):
    #s='for 5 consec:'
    #print s
    
    qualified_stocks=qs_list(year,data,ticks,consecutiveness)
    
    l=portfolio(str(year),data,qualified_stocks,consecutiveness)
    print 'Return in '+str(year)+' is %f'%l[0]
    print 'Sharpe in '+str(year)+' is %f'%l[1]
    
    return qualified_stocks

#%%insert corresponding year to get portfolio data. uncomment the for loops to see 
#%%qualified stocks. True is bullish and false is bearish for the last parameter
consecutiveness=5
qualified_stocks=main(2016,data,ticks,consecutiveness)
for item in qualified_stocks: print item

for year in range(2011,2016):
    consecutiveness = 5
    qualified_stocks=main(year,data,ticks,consecutiveness)
    

#qualified_stocks=main(2015,data,ticks)
#for item in qualified_stocks: print item

#qualified_stocks=main(2015,data,ticks)
#for item in qualified_stocks: print item